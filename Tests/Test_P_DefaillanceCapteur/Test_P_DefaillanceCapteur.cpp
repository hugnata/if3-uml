/*************************************************************************
                           Xxx  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <Xxx> (fichier Xxx.cpp) ---------------
using namespace std;
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <time.h>
//------------------------------------------------------ Include personnel
#include "../../Reader.h"
#include "../../Measure.h"
#include "../../Sensor.h"
#include "../../Statistics.h"
///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main()
// Algorithme :
//
{
    cout << "Démarrage des tests de performance pour la défaillance des capteurs" << endl;
    //Création des objets nécessaires
    Reader reader = Reader();
    measureMap_t measureMap;
    sensorVector_t sensorVec;
    attributeVector_t attributeVec;
    string okGreen = " \x1B[32m[OK]\033[0m\t\t ";
    string yellow = "\x1B[33m";
    string endColor = "\033[0m";

    reader.loadSensors("../Data/sensors.csv", sensorVec);
    reader.loadAttributes("../Data/AttributeType.csv", attributeVec);
    reader.loadMeasures("../Data/data_10sensors_1year.csv", measureMap, sensorVec, attributeVec);

    Statistics stats;
    stats.setAttributesConcerned(attributeVec);
    stats.setSensorsConcerned(sensorVec);
    stats.calculateAverage(measureMap);
    clock_t depart = clock();//Depart du test
    stats.detectFailingSensors(measureMap, 40);
    clock_t fin = clock();
    cout << "Test detection: " << flush ;
    if((((double)fin-depart)/ CLOCKS_PER_SEC)>15){
      return 1;
    }else{
      cout << okGreen << endl;
    }
    cout << "Temps total: " <<  ((double)fin-depart)/ CLOCKS_PER_SEC << " s"<< endl;
    return 0;

} //----- fin de Nom
