/*************************************************************************
                           Xxx  -  description
                             -------------------
    d��but                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- R��alisation du module <Xxx> (fichier Xxx.cpp) ---------------
using namespace std;
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include syst��me
#include <iostream>
#include <vector>
#include <unordered_map>
# include <math.h>
//------------------------------------------------------ Include personnel
#include "../../Reader.h"
#include "../../Measure.h"
#include "../../Sensor.h"
#include "../../Statistics.h"
///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types
/* // dans le Reader.h
typedef vector<Measure> measureVector_t;
typedef unordered_map<string, measureVector_t> attributeMap_t;
typedef unordered_map<string, attributeMap_t> measureMap_t;
typedef pair<string, attributeMap_t> paireSensorAtt_t;
typedef vector<Sensor> sensorVector_t;
typedef vector<Attribute> attributeVector_t;
*/

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions priv��es
//static type nom ( liste de param��tres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main ()
// Algorithme :
//
{
    cout << "D��marrage des tests unitaires pour la detection de similarite par DetectionSimilarite() " << endl;
    //Cr��ation des objets n��cessaires
	Reader reader = Reader();
	measureMap_t measureMap;
	sensorVector_t sensorVec;
	attributeVector_t attributeVec;
	Statistics stats;

	uint timeRange;
	uint deltaMeasure;
	vector<pair<Sensor,Sensor>> setPairs;

    string okGreen = " \x1B[32m[OK]\033[0m\t\t ";
    string yellow = "\x1B[33m";
    string endColor = "\033[0m";

    //chargement des fichiers
    cout<<"Ouverture du fichier sensor.csv: "<<flush;
    reader.loadSensors("../Data/sensors.csv",sensorVec);
    cout<<okGreen<<endl;

    cout<<"Ouverture du fichier AttributeType.csv: "<<flush;
    reader.loadAttributes("../Data/AttributeType.csv",attributeVec);
    cout<<okGreen<<endl;

    cout<<"Ouverture du fichier data.csv: "<<flush;
    pair<time_t,time_t> range = reader.loadMeasures("../Data/similarite.csv",measureMap,sensorVec,attributeVec);
    stats.setMeasureRange(range);
    cout<<okGreen<<endl;

    //associer les attributs et les capteurs au stats
    stats.addAttributeConcerned(attributeVec[0]);
    for(int i = 0;i<3;i++)
	    stats.addSensorConcerned(sensorVec[i]);

    //tests
    cout << yellow << "Test de la fonction DectectionSimilarite()"<<endColor<< endl;
    cout << "Test #1: " << flush;
    cout << "critère strict (delta = 0) => pas de similarité" << flush;
    timeRange = 4;
    deltaMeasure = 0;
    setPairs = stats.detectSimilarities(measureMap,timeRange,deltaMeasure);
    if(!setPairs.empty()) // si on detecte des similarites pour delta = 0
	    return 1;
    cout << okGreen << endl;

    cout << "Test #2:" << flush;
    cout << "critère pas strict du tout(delta = 10) => toutes les paires conviennent" <<flush;
    timeRange = 4;
    deltaMeasure = 10;
    setPairs = stats.detectSimilarities(measureMap, timeRange, deltaMeasure);
    if(setPairs.size() != 3)
	    return 1;
    cout << okGreen << endl;

    cout << "Test #3:" << flush;
    cout << "test avec delta = 2 et range = 2 => une paire de capteurs similaires" << flush;
    timeRange = 2;
    deltaMeasure = 2;
    setPairs = stats.detectSimilarities(measureMap, timeRange, deltaMeasure);
    if(setPairs.size()!=1)
	    return 1;
    cout << okGreen << endl;


    return 0;
} //----- fin de Nom
