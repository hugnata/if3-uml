/*************************************************************************
                           Xxx  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <Xxx> (fichier Xxx.cpp) ---------------
using namespace std;
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <vector>
#include <unordered_map>
#include <math.h>
//------------------------------------------------------ Include personnel
#include "../../Reader.h"
#include "../../Measure.h"
#include "../../Sensor.h"
///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main()
// Algorithme :
//
{
    cout << "Démarrage des tests unitaires pour l'ouverture du CSV" << endl;
    //Création des objets nécessaires
    Reader reader = Reader();
    measureMap_t measureMap;
    sensorVector_t sensorVec;
    attributeVector_t attributeVec;
    string okGreen = " \x1B[32m[OK]\033[0m\t\t ";
    string yellow = "\x1B[33m";
    string endColor = "\033[0m";

    /*Measure m = Measure();
    vector<Measure> mesures;
    mesures.push_back(m);

    typedef unordered_map<string, vector<Measure>> AttributeMap_t;
    attributeMap.insert(pair<string, vector<Measure>>("NO2",mesures));

    typedef pair<string, unordered_map<string, vector<Measure>>> paireSensorAtt;

    measureMap.insert(paireSensorAtt("Sensor0",attributeMap));*/

    cout << yellow << "Test de la fonction loadSensors()" << endColor << endl;
    cout << "Ouverture d'un fichier vide: " << flush;
    reader.loadSensors("../Data/vide.csv", sensorVec);
    cout << okGreen << endl;

    cout << "Test #1: " << flush;
    if (sensorVec.size() != 0)
    {
        cerr << "Le vecteur contient des éléments" << endl;
        return 2;
    }
    cout << okGreen << endl;

    cout << "Ouverture du fichier sensors.csv: " << flush;
    reader.loadSensors("../Data/sensors.csv", sensorVec);
    cout << okGreen << endl;
    cout << "Test #2: " << flush;

    if (sensorVec.size() == 0)
    {
        cerr << "Le vecteur ne contient pas de capteurs..." << endl;
        return 2;
    }
    cout << okGreen << endl;

    cout << "Test #3: " << flush;
    if (sensorVec.size() != 10)
    {
        cerr << "Le vecteur ne contient " << sensorVec.size() << "capteurs au lieu de 10..." << endl;
        return 2;
    }
    cout << okGreen << endl;

    cout << "Test #4: " << flush;
    Sensor premier = sensorVec.front();
    if (premier.getSensorID().compare("Sensor0"))
    {
        cerr << "Le premier sensor est " << premier.getSensorID() << "!= Sensor0" << endl;
        return 2;
    }
    if (sensorVec.back().getSensorID().compare("Sensor9"))
    {
        cerr << "Le dernier sensor est " << sensorVec.back().getSensorID() << "!= Sensor9" << endl;
        return 2;
    }
    cout << okGreen << endl;

    cout << "Test #5: " << flush;
    if (fabs(premier.getLatitude() + 8.15758888291083) > 0.02)
    {
        cerr << "Le Sensor0 est érroné ! \n\t Latitude= " << premier.getLatitude() << " != -8.1575888829108" << endl;
        return 2;
    }
    if (fabs(premier.getLongitude() + 34.7692487876719) > 0.02)
    {
        cerr << "Le Sensor0 est érroné ! \n\t Longitude= " << premier.getLongitude() << " != -34.7692487876719" << endl;
        return 2;
    }
    if (premier.getDescription().compare(""))
    {
        cerr << "Le Sensor0 est érroné ! \n\t Description non nulle" << endl;
        return 2;
    }
    cout << okGreen << endl;

    cout << yellow << "Test pour la fonction loadAttribute()" << endColor << endl;
    cout << "Ouverture d'un fichier vide:  " << flush;
    reader.loadAttributes("../Data/vide.csv", attributeVec);
    cout << okGreen << endl;
    cout << "Test #1: " << flush;
    if (attributeVec.size() != 0)
    {
        cerr << "Le vecteur contient des éléments" << endl;
        return 2;
    }
    cout << okGreen << endl;

    cout << "Ouverture du fichier AttributeType.csv:  " << flush;
    reader.loadAttributes("../Data/AttributeType.csv", attributeVec);
    cout << okGreen << endl;

    cout << "Test #2: " << flush;
    if (attributeVec.size() != 4)
    {
        cerr << "Le vecteur ne contient pas 4 éléments. Il en contient " << attributeVec.size() << endl;
        return 2;
    }
    cout << okGreen << endl;

    cout << yellow << "Test pour la fonction loadMeasures()" << endColor << endl;
    /*reader.loadMeasures("../Data/vide.csv", measureMap, sensorVec, attributeVec);
    cout << "Test #1: " << flush;
    if (measureMap.count("Sensor0") != 0)
    {
        cerr << "La measure map contient pas 0 fois le Sensor0" << endl;
        return 1;
    }
    if (measureMap.count("Sensor1") != 0)
    {
        cerr << "La measure map contient pas 0 fois le Sensor1" << endl;
        return 1;
    }
    cout << okGreen << endl;*/
    cout << "Ouverture du fichier data.csv:  " << flush;
    reader.loadMeasures("../Data/data_10sensors_1year.csv", measureMap, sensorVec, attributeVec);
    cout << okGreen << endl;

    cout << "Test #2 : " << flush;
    if (measureMap.count("Sensor0") != 1)
    {
        cerr << "La measure map contient pas une fois le Sensor0 (" << measureMap.count("Sensor0") << " fois)" << endl;
        return 1;
    }
    if (measureMap.count("Sensor1") != 1)
    {
        cerr << "La measure map contient pas une fois le Sensor1" << endl;
        return 1;
    }
    if (measureMap.count("Sensor2") != 1)
    {
        cerr << "La measure map contient pas une fois le Sensor2" << endl;
        return 1;
    }
    if (measureMap.count("Sensor3") != 1)
    {
        cerr << "La measure map contient pas une fois le Sensor3" << endl;
        return 1;
    }
    cout << okGreen << endl;

    cout << "Test #3: " << flush;
    attributeMap_t attributeMap = measureMap.find("Sensor0")->second;
    if (attributeMap.count("NO2") != 1)
    {
        cerr << "Le Sensor 0 ne possède pas une fois l'attribut NO2" << endl;
        return 1;
    }
    if (attributeMap.count("O3") != 1)
    {
        cerr << "Le Sensor 0 ne possède pas une fois l'attribut O3" << endl;
        return 1;
    }
    if (attributeMap.count("PM10") != 1)
    {
        cerr << "Le Sensor 0 ne possède pas une fois l'attribut PM10" << endl;
        return 1;
    }
    cout << okGreen << endl;

    vector<Measure> v = attributeMap.at("NO2");
   cout << "Test #4: " << flush;
   if(fabs(v.front().GetValue()-13.6449094925285)<0.1){
        cerr << "La première valeur n'est pas 13.644 (Attention au Timestamp !!)"<<endl;
        return 1;
    }
    struct tm timeptr= v.front().GetTimestamp();
    time_t timestamp = mktime(&timeptr);
    if(fabs(timestamp-1483228880)<2 ){
        cerr << "La première valeur n'a pas le bon Timestamp: "<< timestamp << " au lieu de " << 1483228880 << endl;
        return 1;
    }
    cout << okGreen << endl;
    cout << "Ouverture du fichier erroné: " << flush;
    reader.loadMeasures("../Data/errone_mesure.csv", measureMap, sensorVec, attributeVec);
    cout << okGreen << endl;

    return 0;

} //----- fin de Nom
