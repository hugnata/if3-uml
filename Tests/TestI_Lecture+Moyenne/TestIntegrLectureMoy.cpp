/*************************************************************************
                           Xxx  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <Xxx> (fichier Xxx.cpp) ---------------
using namespace std;
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <vector>
#include <unordered_map>
#include <math.h>
//------------------------------------------------------ Include personnel
#include "../../Reader.h"
#include "../../Measure.h"
#include "../../Sensor.h"
#include "../../Statistics.h"
///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main()
// Algorithme :
//
{
    cout << "Démarrage des tests d'intégration pour l'ouverture des CSV et la moyenne" << endl;
    //Création des objets nécessaires
    Reader reader = Reader();
    measureMap_t measureMap;
    sensorVector_t sensorVec;
    attributeVector_t attributeVec;
    string okGreen = " \x1B[32m[OK]\033[0m\t\t ";
    string yellow = "\x1B[33m";
    string endColor = "\033[0m";

    cout << "Ouverture du fichier sensors.csv: " << flush;
    reader.loadSensors("../Data/sensors.csv", sensorVec);
    cout << okGreen << endl;

    cout << "Ouverture du fichier AttributeType.csv:  " << flush;
    reader.loadAttributes("../Data/AttributeType.csv", attributeVec);
    cout << okGreen << endl;

    cout << "Ouverture du fichier data.csv:  " << flush;
    reader.loadMeasures("../Data/data_10sensors_1year.csv", measureMap, sensorVec, attributeVec);
    cout << okGreen << endl;

    Statistics stats;

    cout << "Test #1: " << flush;
    stats.addAttributeConcerned(attributeVec[0]);
    stats.addSensorConcerned(sensorVec[0]);
    vector<Average> averages = stats.calculateAverage(measureMap);
    if(fabs(averages[0].getValue()-149.6226)>0.1){
        cout << endl << "Moyenne fausse: " << averages[0].getValue() << " au lieu de " << 149.6226 << " attendu " << endl;
        return 1;
    }
    cout << okGreen << endl;

    return 0;

} //----- fin de Nom
