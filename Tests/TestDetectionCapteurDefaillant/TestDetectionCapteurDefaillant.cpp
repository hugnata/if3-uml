/*************************************************************************
                           Xxx  -  description
                             -------------------
    d��but                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- R��alisation du module <Xxx> (fichier Xxx.cpp) ---------------
using namespace std;
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include syst��me
#include <iostream>
#include <vector>
#include <unordered_map>
# include <math.h>
//------------------------------------------------------ Include personnel
#include "../../Reader.h"
#include "../../Measure.h"
#include "../../Sensor.h"
#include "../../Statistics.h"
///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types
/* // dans le Reader.h
typedef vector<Measure> measureVector_t;
typedef unordered_map<string, measureVector_t> attributeMap_t;
typedef unordered_map<string, attributeMap_t> measureMap_t;
typedef pair<string, attributeMap_t> paireSensorAtt_t;
typedef vector<Sensor> sensorVector_t;
typedef vector<Attribute> attributeVector_t;
*/

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions priv��es
//static type nom ( liste de param��tres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main ()
// Algorithme :
//
{
    cout << "Demarrage des tests unitaires pour la detection de similarite par DetectionSimilarite() " << endl;
    //Creation des objets n��cessaires
	Reader reader = Reader();
	measureMap_t measureMap;
	sensorVector_t sensorVec;
	attributeVector_t attributeVec;
	Statistics stats;

	uint deltaT;
	vector<pair<Sensor,string>> setPairs;

    string okGreen = " \x1B[32m[OK]\033[0m\t\t ";
    string yellow = "\x1B[33m";
    string endColor = "\033[0m";

    cout<<"Ouverture du fichier sensor.csv: "<<flush;
    reader.loadSensors("../Data/sensors.csv",sensorVec);
    cout<<okGreen<<endl;

    cout<<"Ouverture du fichier AttributeType.csv: "<<flush;
    reader.loadAttributes("../Data/AttributeType.csv",attributeVec);
    cout<<okGreen<<endl;

    cout << yellow << "Test de la fonction DectectionSimilarite()"<<endColor<< endl;

    // test sur un capteur qui manque des valeurs
    cout << "Test #1: " << flush;
    cout << " test sur un capteur qui manque des mesures " << endl;
    stats.clear();
    stats.addAttributeConcerned(attributeVec[0]);
	stats.addSensorConcerned(sensorVec[0]);
    deltaT = 1;
    
    setPairs = stats.detectFailingSensors(measureMap,deltaT);
    if(setPairs.empty())
        return 1;
    else
        cout << setPairs[0].second << endl;
    cout << okGreen << endl;

    // test sur des capteurs qui n'ont pas enregistré des valeurs de l'attribut donné
    cout << "Test #2: " << flush;
    cout << " test sur des capteurs qui enregistrent des valeurs pour un mauvais attribut " << endl;
    cout << "Ouverture du fichier ntm.csv: " << flush;
    reader.loadMeasures("../Data/ntm.csv",measureMap,sensorVec,attributeVec);
    cout << okGreen<<endl;
    stats.clear();
    stats.addAttributeConcerned(attributeVec[1]);
    for(int i = 0;i < 3;i++)
	    stats.addSensorConcerned(sensorVec[i]);
    deltaT = 1;
    
    setPairs = stats.detectFailingSensors(measureMap,deltaT);
    cout << "il y a " << setPairs.size() <<" défaillances" << flush;
    if(setPairs.empty())
        return 1;
    else
    {
        cout << ":" << endl;
        for(int i = 0; i < setPairs.size();i++)
            cout << setPairs[i].first.getSensorID() << " a défailli: " <<setPairs[i].second << endl;
        
    }
    cout << okGreen << endl;

    // test sur des capteurs dont un capteur défaillant
    cout << "Test #3: " << flush;
    cout << " test sur des capteurs " << endl;
    cout<<"Ouverture du fichier ntm.csv: "<<flush;
   
    reader.loadMeasures("../Data/ntm.csv",measureMap,sensorVec,attributeVec);
    cout<<okGreen<<endl;
    
    stats.clear();
    stats.addAttributeConcerned(attributeVec[0]);
	stats.addSensorConcerned(sensorVec[0]);
    deltaT = 2;
    setPairs = stats.detectFailingSensors(measureMap,deltaT);
    cout << "il y a " << setPairs.size() <<" défaillances" << flush;
    if(!setPairs.empty())
    {
        cout << ":" << endl;
        for(int i = 0; i < setPairs.size();i++)
            cout << setPairs[i].first.getSensorID() << " a défailli: " <<setPairs[i].second << endl;
    }
    else
    {
        return 1;
    }
    cout << okGreen << endl;

    return 0;

} //----- fin de Nom
