/*************************************************************************
                           Xxx  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <Xxx> (fichier Xxx.cpp) ---------------
using namespace std;
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <time.h>
//------------------------------------------------------ Include personnel
#include "../../Reader.h"
#include "../../Measure.h"
#include "../../Sensor.h"
#include "../../Statistics.h"
///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main()
// Algorithme :
//
{
    cout << "Démarrage des tests de performance pour l'ouverture du CSV et le calcul de la moyenne" << endl;
    //Création des objets nécessaires
    Reader reader = Reader();
    measureMap_t measureMap;
    sensorVector_t sensorVec;
    attributeVector_t attributeVec;
    string okGreen = " \x1B[32m[OK]\033[0m\t\t ";
    string yellow = "\x1B[33m";
    string endColor = "\033[0m";

    /*Measure m = Measure();
    vector<Measure> mesures;
    mesures.push_back(m);

    typedef unordered_map<string, vector<Measure>> AttributeMap_t;
    attributeMap.insert(pair<string, vector<Measure>>("NO2",mesures));

    typedef pair<string, unordered_map<string, vector<Measure>>> paireSensorAtt;

    measureMap.insert(paireSensorAtt("Sensor0",attributeMap));*/
    clock_t depart = clock();//Depart du test
    reader.loadSensors("../Data/sensors.csv", sensorVec);
    clock_t loadSensor = clock();
    reader.loadAttributes("../Data/AttributeType.csv", attributeVec);
    clock_t loadAttribute = clock();
    pair<time_t,time_t> range = reader.loadMeasures("../Data/data_10sensors_1year.csv", measureMap, sensorVec, attributeVec);
    clock_t loadMeasures = clock();
    Statistics stats;
    stats.setMeasureRange(range);
    stats.setAttributesConcerned(attributeVec);
    stats.setSensorsConcerned(sensorVec);
    stats.calculateAverage(measureMap);
    clock_t moyenne = clock();
    cout << yellow << "Ouverture des fichiers: " << flush;
    //Si l'ouverture des fichiers met moins de 10s
    if(((double)loadMeasures-depart)/ CLOCKS_PER_SEC<10){
      cout << okGreen << endl;
    } else{
      return 1;
    }
    cout << "Ouverture de sensors.csv: "  << ((double)loadSensor-depart)/ CLOCKS_PER_SEC << " s"<< endl;
    cout << "Ouverture de AttributeType.csv: "  << ((double)loadAttribute-loadSensor)/ CLOCKS_PER_SEC << " s"<< endl;
    cout << "Ouverture de data.csv: "  << ((double)loadMeasures-loadAttribute)/ CLOCKS_PER_SEC << " s"<< endl;
    cout << yellow << "Calcul de la moyenne: " << flush;
    //Si l'ouverture des fichiers met moins de 10s
    if(((double)moyenne-loadMeasures)/ CLOCKS_PER_SEC<3){
      cout << okGreen << endl;
    } else{
      return 1;
    }
    cout << "Calcul de la moyenne: "  << ((double)moyenne-loadMeasures)/ CLOCKS_PER_SEC << " s"<< endl;
    cout << "Temps total: " <<  ((double)moyenne-depart)/ CLOCKS_PER_SEC << " s"<< endl;

    return 0;

} //----- fin de Nom
