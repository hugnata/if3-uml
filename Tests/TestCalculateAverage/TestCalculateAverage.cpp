/*************************************************************************
                           Xxx  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <Xxx> (fichier Xxx.cpp) ---------------
using namespace std;
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <vector>
#include <unordered_map>
# include <math.h>
//------------------------------------------------------ Include personnel
#include "../../Reader.h"
#include "../../Measure.h"
#include "../../Sensor.h"
#include "../../Statistics.h"
///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types
/* // dans le Reader.h
typedef vector<Measure> measureVector_t;
typedef unordered_map<string, measureVector_t> attributeMap_t;
typedef unordered_map<string, attributeMap_t> measureMap_t;
typedef pair<string, attributeMap_t> paireSensorAtt_t;
typedef vector<Sensor> sensorVector_t;
typedef vector<Attribute> attributeVector_t;
*/

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main ()
// Algorithme :
//
{
    cout << "Démarrage des tests unitaires pour le calcul de moyenne par calculateAverage() " << endl;
    //Création des objets nécessaires

	Sensor sensor0 = Sensor("sensor0", 0.0, 0.0, "");
	Sensor sensor1 = Sensor("sensor1", 0.0, 0.0, "");// utilisé à partir du test 3
	Attribute attribute0 = Attribute("attribute0","","");
	Attribute attribute1 = Attribute("attribute1","",""); // utilisé à partir du test 3

  	//std::string attributeID = "attribute0";

	measureMap_t datas;
	Statistics stats;
	stats.setTemporalFilter(false);


	// création du vecteur de mesures
	measureVector_t v0_0;
	struct tm timeStamp;
	for(int i=0; i<=10 ; i++){
		v0_0.push_back(Measure(timeStamp, (double)i));
	}

	measureVector_t v0_1;
	for(int i=10; i<=20 ; i++){
		v0_1.push_back(Measure(timeStamp, (double)i));
	}

	measureVector_t v1_0;
	for(int i=20; i<=30 ; i++){
	  v1_0.push_back(Measure(timeStamp, (double)i));
	}

	measureVector_t v1_1;
	for(int i=30; i<=40 ; i++){
	  v1_1.push_back(Measure(timeStamp, (double)i));
	}

	// création de la map de chaque sensor attMap = un essemble de paire (attribut ; Vecteur de mesures)
    attributeMap_t attMapSensor0;
    attMapSensor0.insert(pair<string, measureVector_t>(attribute0.getAttributeID(), v0_0));
    attributeMap_t attMapSensor1; // utilisé à partir du test 3
    attMapSensor1.insert(pair<string, measureVector_t>(attribute0.getAttributeID(), v1_0));
    attMapSensor1.insert(pair<string, measureVector_t>(attribute1.getAttributeID(), v1_1));


    // ajout de la map associée à la clé du sensor
    datas.insert(paireSensorAtt_t(sensor0.getSensorID(), attMapSensor0));
    datas.insert(paireSensorAtt_t(sensor1.getSensorID(), attMapSensor1));


    // variable pour stoquer le retour de methode calculateAverage:
    vector<Average> averages;
    vector<Average>::iterator it;


    string okGreen = " \x1B[32m[OK]\033[0m\t\t ";
    string yellow = "\x1B[33m";
    string endColor = "\033[0m";
  	string failYellow = "\x1B[33m[FAIL]\033[0m";




    cout << yellow << "Test de la fonction CalculateAverage()"<< endColor << endl;



    cout << "Test #1: " << flush;
    cout << " sensorsConcerned vide: " << flush;
    stats.addAttributeConcerned(attribute0);
    if(stats.calculateAverage(datas).empty() == false){
	  cout << failYellow <<endl;
      cerr << "Une moyenne a été calculée sans capteur !" << endl;
      return 1;
    }else{
	    cout << okGreen <<endl;
	}


    cout << "Test #2: " << flush;
    cout << " setAttributeConcerned vide: " << flush;
    stats.addSensorConcerned(sensor0);
    Attribute nullAtt;
    stats.setAttributesConcerned(vector<Attribute>());
    if(stats.calculateAverage(datas).empty() == false){
      cout << failYellow <<endl;
      cerr << "Une moyenne a été calculée sans attribut ! " << endl;
      return 1;
    }else{
	    cout << okGreen <<endl;
	}

	cout << "Test #3 - " << flush;
    cout << " calcul de la moyenne sur 0 1 2 3 4 5 6 7 8 9 10: " << flush;
    stats.resetAttributesConcerned();
    stats.addAttributeConcerned(attribute0);
    averages = stats.calculateAverage(datas);
    if(averages.empty()==false){
	    it = averages.begin();
	      //cout << " it ok ! "<< endl;  // ok l'iterateur fonctionne !
    	double avgCalculated = it->getValue(); // ca ne marche pas "Erreur de segmentation (core dumped)"

    	if (avgCalculated == 5.0) {
        cout << okGreen <<endl;

    	}else{
    		cout << failYellow <<endl;
      		cerr << " Calcul de moyenne sur 11 valeurs echec " << endl;
    		cerr << " moyenne calculée : " <<avgCalculated<< endl;
    		cerr << " moyenne attendue : 5.0" << endl;
			return 1;
    	}



	}else{
      cout << failYellow <<endl;
      cerr << "Aucune moyenne n'a été calculée ! " <<endl;
      return 1;
	}



// faire des tests avec 2 attributs
  cout << "Test #4 - " << flush;
	cout << " calcul de la moyenne sur 2 attributs 1 sensor " << flush;
	stats.resetAttributesConcerned();
	attMapSensor0.insert(pair<string, measureVector_t>(attribute1.getAttributeID(), v0_1));
	stats.addAttributeConcerned(attribute0);
	stats.addAttributeConcerned(attribute1);
	// mise a jour des donnée :
	datas.at(sensor0.getSensorID()) = attMapSensor0;

	averages = stats.calculateAverage(datas);

	if (averages.size() != 2){
	  cout << failYellow <<endl;
	  cerr << " nombre de moyennes calculée pas coherent: "<<averages.size()<<" moyennes calculé au lieu de 2"<<endl;
	}

	it = averages.begin();
	double avgCalculated0_0 = it->getValue();
	//it.next();
	double avgCalculated0_1 = (it+1)->getValue();

	if (avgCalculated0_0 == 5.0 && avgCalculated0_1 == 15.0) {
	  cout << okGreen <<endl;
	}else{
	  cout << failYellow <<endl;
	  cerr << "Moyennes calculées sont érronées ! " <<endl;
	  cout << "avgCalculated0_0 : "<<avgCalculated0_0<<" au lieu de 5.0"<<endl;
	  cout << "avgCalculated0_1 : "<<avgCalculated0_1<<" au lieu de 15.0"<<endl;
	  return 1;
	}




// faire des tests avec 2 sensors
  cout << "Test #5 - " << flush;
  cout << " calcul de la moyenne sur 2 sensors " << flush;
  stats.clear();
  stats.addSensorConcerned(sensor0);
  stats.addSensorConcerned(sensor1);
  stats.addAttributeConcerned(attribute0);
  stats.addAttributeConcerned(attribute1);

  averages = stats.calculateAverage(datas);

  if (averages.size() != 4){
    cout << failYellow <<endl;
    cerr << " nombre de moyennes calculée pas coherent: "<<averages.size()<<" moyennes calculé au lieu de 4"<<endl;
  }

  double avgsCalculated[4];
  bool resultatCorrect = true;
  for (uint i=0; i<averages.size() ; i++)
  {
    avgsCalculated[i] = averages[i].getValue();
    if(avgsCalculated[i] != (double)(5+i*10) )
      resultatCorrect=false;
  }

  if (resultatCorrect){
    cout << okGreen <<endl;
  }else{
    cout << failYellow <<endl;
    cerr << "Moyennes calculées sont érronées ! " <<endl;
    for (uint i=0; i<averages.size() ; i++){
      cout << "avgsCalculated["<<i<<"] : "<<averages[i].getValue()<<" au lieu de "<<(double)(5+i*10)<<endl;
    }
    cout << "Resultat : " <<endl;
    for (uint i=0; i<averages.size() ; i++){
      cout << "avgsCalculated["<<i<<"] : ["<<averages[i].getSensor().getSensorID()<< ","<<averages[i].getAttribute().getAttributeID() << "," << averages[i].getValue() << "]"  << endl;
    }

    return 1;
  }




return 0;

} //----- fin de Nom
