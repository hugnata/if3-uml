/*************************************************************************
                           Reader  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Reader> (fichier Reader.h) ----------------
#if !defined(READER_H)
#define READER_H

//--------------------------------------------------- Interfaces utilisées
#include <fstream>
#include <unordered_map>
#include <list>
#include <vector>
#include <string>
#include <utility>
#include <time.h>
#include "Measure.h"
#include "Sensor.h"
#include "Attribute.h"
using namespace std;
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types
typedef vector<Measure> measureVector_t;
typedef unordered_map<string, measureVector_t> attributeMap_t;
typedef unordered_map<string, attributeMap_t> measureMap_t;
typedef pair<string, attributeMap_t> paireSensorAtt_t;
typedef vector<Sensor> sensorVector_t;
typedef vector<Attribute> attributeVector_t;
//------------------------------------------------------------------------
// Rôle de la classe <Reader>
//
// Extrait les données d'un CSV et les range dans une structure
//
//------------------------------------------------------------------------

class Reader : public ifstream
{
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques
    pair<time_t, time_t> loadMeasures(const string &fileName, measureMap_t &datas, const sensorVector_t &sensors, const attributeVector_t &attributes);
    // Mode d'emploi :
    // Charge les mesures du fichier fourni en entrée
    // dans la structure de donnée fournie
    //
    // Contrat :
    // Les mesures doivent être triés par ordre chronologique croissant

    void loadSensors(const string &fileName, sensorVector_t &sensors);
    // Mode d'emploi :
    // Charge les capteurs du fichier fourni en entrée
    // dans la structure de donnée fournie
    //

    void loadAttributes(const string &fileName, attributeVector_t &attributes);
    // Mode d'emploi :
    // Charge les attributs du fichier fourni en entrée
    // dans la structure de donnée fournie
    //

    //-------------------------------------------- Constructeurs - destructeur
    Reader(const Reader &aReader);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Reader();
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Reader();
    // Mode d'emploi :
    //
    // Contrat :
    //

    //------------------------------------------------------------------ PRIVE

protected:
    //----------------------------------------------------- Méthodes protégées

    void nextMeasureUTF16(struct tm &t, double &value, string &sensorID, string &attributeID);
    // Mode d'emploi :
    // Permet la lecture d'une ligne en ouvrant le fichier en UTF16
    // (souci rencontré par le groupe)
    //

    void nextMeasureUTF8(struct tm &t, double &value, string &sensorID, string &attributeID);
    // Mode d'emploi :
    // Permet la lecture d'une ligne en ouvrant le fichier en UTF8
    // (A implémenter au besoin)
    //

    //----------------------------------------------------- Attributs protégés
};

//-------------------------------- Autres définitions dépendantes de <Reader>

#endif // READER_H
