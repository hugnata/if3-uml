/*************************************************************************
                           Statistics  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Statistics> (fichier Statistics.h) ----------------
#if !defined(STATISTICS_H)
#define STATISTICS_H

//--------------------------------------------------- Interfaces utilisées
#include "Attribute.h"
#include "Sensor.h"
#include "Average.h"
#include "Reader.h"
#include <vector>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Statistics>
//
//
//------------------------------------------------------------------------
class Statistics
{
  //----------------------------------------------------------------- PUBLIC

public:
  //----------------------------------------------------- Méthodes publiques
  std::vector<std::pair<Sensor, std::string>> detectFailingSensors(measureMap_t &measureMap, int deltaT);
  // Mode d'emploi :
  // Détecte les capteurs défaillants (Plus d'infos sur la description des algorithmes)
  // Contrat :
  //

  std::vector<Average> calculateAverage(const measureMap_t &measureMap);
  // Mode d'emploi :
  // Retourne la moyenne sur les capteurs choisis et l'attribut choisi. Si l'un des deux n'est pas défini, on renvoie Null
  // Contrat :
  //

  std::vector<std::pair<Sensor, Sensor>> detectSimilarities(const measureMap_t &measureMap, uint timeRange,uint deltaMesure);
  // Mode d'emploi :
  // Retourne les capteurs qui enregistrent des données similaires
  // Contrat :
  //

  void clear();
  // Mode d'emploi :
  //
  // Contrat :
  //

  //-------------------------------------------- Constructeurs - destructeur
  Statistics();
  // Mode d'emploi :
  //
  // Contrat :
  //

  ~Statistics();
  // Mode d'emploi :
  //
  // Contrat :
  //

  //------------------------------------------------------------------ Accesseurs

  //Attribute Concerned
  void setAttributesConcerned(const std::vector<Attribute> &attributesConcerned)
  {
    this->attributesConcerned = attributesConcerned;
  } //------ Fin

  void addAttributeConcerned(const Attribute &attributeConcerned)
  {
    this->attributesConcerned.push_back(attributeConcerned);
  } //------ Fin

  std::vector<Attribute> getAttributesConcerned() const
  {
    return this->attributesConcerned;
  } //------ Fin

  void resetAttributesConcerned()
  {
    this->attributesConcerned = vector<Attribute>();
  } //------ Fin

  // SensorConcerned
  void setSensorsConcerned(const sensorVector_t &sensorsConcerned)
  {
    this->sensorsConcerned = sensorsConcerned;
  } //------ Fin

  sensorVector_t getSensorsConcerned() const
  {
    return this->sensorsConcerned;
  } //------ Fin

  void addSensorConcerned(const Sensor &sensorConcerned)
  {
    this->sensorsConcerned.push_back(sensorConcerned);
  } //------ Fin

  // TemporalFilter
  void setTemporalFilter(const bool &temporalFilter)
  {
    this->temporalFilter = temporalFilter;
  } //------ Fin

  bool getTemporalFilter() const
  {
    return this->temporalFilter;
  } //------ Fin

  // beginTimestamp
  void setBeginTimestamp(struct tm &beginTimestamp)
  {
    this->beginTimestamp = mktime(&beginTimestamp);
  } //------ Fin

  void setBeginTimestamp(time_t beginTimestamp)
  {
    this->beginTimestamp = beginTimestamp;
  } //------ Fin

  void setBeginTimestamp()
  {
    this->beginTimestamp = this->firstMeasure;
  } //------ Fin

  time_t getBeginTimestamp() const
  {
    return this->beginTimestamp;
  } //------ Fin

  //endTimestamp
  void setEndTimestamp(struct tm &endTimestamp)
  {
    this->endTimestamp = mktime(&endTimestamp);
  } //------ Fin

  void setEndTimestamp(time_t endTimestamp)
  {
    this->endTimestamp = endTimestamp;
  } //------ Fin

  void setEndTimestamp()
  {
    endTimestamp = lastMeasure;
  } //------ Fin

  time_t getEndTimestamp() const
  {
    return this->endTimestamp;
  } //------ Fin

  // firstMeasure et lastMeasure
  void setMeasureRange(const pair<time_t, time_t> &range)
  {
    firstMeasure = range.first;
    lastMeasure = range.second;
  } //------ Fin

  time_t getFirstMeasure() const
  {
    return firstMeasure;
  } //------ Fin

  time_t getLastMeasure() const
  {
    return lastMeasure;
  } //------ Fin

protected:
  //----------------------------------------------------- Méthodes protégées
  int getFirstDateInRange(measureVector_t &mes);
  // Contrat : Renvoie l'indice de la première valeur dans la plage horaire définie dans Statistics
  //
  int getLastDateInRange(measureVector_t &mes);
  // Contrat : Renvoie l'indice de la dernière valeur dans la plage horaire définie dans Statistics

  //----------------------------------------------------- Attributs protégés
  attributeVector_t attributesConcerned;
  sensorVector_t sensorsConcerned;
  bool temporalFilter;
  time_t beginTimestamp;
  time_t endTimestamp;
  time_t firstMeasure;
  time_t lastMeasure;
};

#endif // STATISTICS_H
