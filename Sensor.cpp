/*************************************************************************
                           Sensor  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Sensor> (fichier Sensor.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <string>
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Sensor.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
bool Sensor::containedInCircle(double latitudeCentre, double longitudeCentre, double radius) const
// Algorithme :
//
{
    double distanceSquare = (latitudeCentre - latitude) * (latitudeCentre - latitude) + (longitudeCentre - longitude) * (longitudeCentre - longitude);
    return (radius * radius >= distanceSquare);
} //----- Fin de Méthode

//-------------------------------------------- Constructeurs - destructeur
Sensor::Sensor(const Sensor &unSensor)
{
#ifdef OTRACE
    cout << "Appel au constructeur de copie de <Sensor>" << endl;
#endif
    this->sensorID = unSensor.getSensorID();
    this->latitude = unSensor.getLatitude();
    this->longitude = unSensor.getLongitude();
    this->description = unSensor.getDescription();
} //----- Fin de Sensor (constructeur de copie)

Sensor::Sensor()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de <Sensor>" << endl;
#endif
} //----- Fin de Sensor

Sensor::Sensor(const string &ID, const double &LATITUDE, const double &LONGITUDE, const string &DESCRIPTION) : sensorID(ID), latitude(LATITUDE), longitude(LONGITUDE), description(DESCRIPTION)
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de <Sensor>" << endl;
#endif
} //----- Fin de Sensor

Sensor::~Sensor()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au destructeur de <Sensor>" << endl;
#endif
} //----- Fin de ~Sensor
