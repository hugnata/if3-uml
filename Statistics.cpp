/*************************************************************************
                           Statistics  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Statistics> (fichier Statistics.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <math.h>
#include <limits>
#include <map>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Statistics.h"
#include "Reader.h"
//------------------------------------------------------------- Constantes
#define ecart_type 5
//----------------------------------------------------------------- PUBLIC
typedef unordered_map<string, measureVector_t> attributeMap_t;
//----------------------------------------------------- Méthodes publiques
// type Statistics::Méthode ( liste des paramètres )
// Algorithme :
//
//{DEFINE
//} //----- Fin de Méthode

vector<Average> Statistics::calculateAverage(const measureMap_t &measureMap)
//Algorithme :
//
{
#ifdef MAP
  clog << "Appel à la fonction <Statistics::calculateAverage>" << endl;
#endif

  vector<Average> averages;

  if (this->sensorsConcerned.size() == 0)
    return averages;
  if (this->attributesConcerned.size() == 0)
    return averages;

  //On parcourt tout les sensors concernés
  for (vector<Sensor>::iterator it = this->sensorsConcerned.begin(); it != this->sensorsConcerned.end(); ++it)
  {
    for (vector<Attribute>::iterator ita = this->attributesConcerned.begin(); ita != this->attributesConcerned.end(); ++ita)
    {
      long double sum = std::numeric_limits<double>::quiet_NaN();
      // On récupère les mesures liées à l'attribut en question et au sensor étudié
      if (measureMap.count(it->getSensorID()) && measureMap.at(it->getSensorID()).count(ita->getAttributeID()))
      {
        sum = 0;
        vector<Measure> measures = measureMap.at(it->getSensorID()).at(ita->getAttributeID()); //On récupère le vecteur de mesures

        if (this->getTemporalFilter())
        {
          //On cherche le début par dichotomie
          int firstMeasure = getFirstDateInRange(measures);
          int lastMeasure = getLastDateInRange(measures);
          for (int i = firstMeasure; i < lastMeasure + 1; i++)
          {
            sum += measures[i].GetValue();
          }
          sum = sum / (lastMeasure - firstMeasure + 1);
        }
        else
        {
          for (uint i = 0; i < measures.size(); i++)
          {
            sum += measures[i].GetValue();
          }
          sum = sum / measures.size();
        }
      }
      Average av = Average(*it, *ita, sum);
      averages.push_back(av);

    }
  }
  return averages;
} //----- Fin de Méthode

vector<pair<Sensor, string>> Statistics::detectFailingSensors(measureMap_t &measureMap, int deltaT)
//Algorithme :
//
{
#ifdef MAP
  clog << "Appel à la fonction <Statistics::detectFailingSensors>" << endl;
#endif
  //On transforme delta T en secondes
  deltaT = deltaT * 60;
  //Les vecteurs qui ne fonctionnent pas seront stocké la
  vector<pair<Sensor, string>> failingSensors;

  vector<Sensor> sensors = this->getSensorsConcerned();
  vector<Attribute> attributes = this->getAttributesConcerned();

  //Pour chaque sensor
  for (vector<Sensor>::iterator sensor = sensors.begin(); sensor < sensors.end(); sensor++)
  {

    //On compte les attributs
    int nbAtt = this->getAttributesConcerned().size();

    //On vérifie que le sensor est dans la map
    if (measureMap.count(sensor->getSensorID()))
    {

      // Stocke, pour un capteur les attributs présents
      bool *attValids = new bool[nbAtt];

      //*********************************Test de la validité de la map *************************************/
      //On parcout ensuite la map rattachée au sensor avec l'iterateur attributeMap
      attributeMap_t::iterator attributeMap = measureMap.at(sensor->getSensorID()).begin();
      for (; attributeMap != measureMap.at(sensor->getSensorID()).end(); attributeMap++)
      {
        bool isValid = false;
        //On vérifie si l'attribut est compris dans attributesToCheck
        for (int i = 0; i < nbAtt; i++)
        {
          //Si l'attribut rattaché au sensor est inclus
          if (this->getAttributesConcerned()[i].getAttributeID().compare(attributeMap->first) == 0)
          {
            for (uint j = 0; j < attributeMap->second.size() - 1; j++)
            {
              if(attributeMap->second[j].GetValue() <0){
                failingSensors.push_back(pair<Sensor, string>(*sensor, "Le capteur a renvoyé une valeur négative !"));
              }
              if (difftime(attributeMap->second[j + 1].GetTimeT(), attributeMap->second[j].GetTimeT()) > deltaT)
              {
                int diffMeas = (difftime(attributeMap->second[j + 1].GetTimeT(), attributeMap->second[j].GetTimeT()) / 60);
                string msg = "Deux mesures ont ";
                msg += to_string(diffMeas);
                msg += " minutes d'intervalle";
                msg += " (attribut: ";
                msg += attributeMap->first;
                msg += ")";
#ifdef STATS
                clog << "Capteur " << sensor->getSensorID() << ": " << msg << endl;
#endif
                failingSensors.push_back(pair<Sensor, string>(*sensor, msg));
                break;
              }
            }
            attValids[i] = true; //On a un attribut valide en plus
            isValid = true;
            break;
          }
        }
        if (!isValid)
        {
          failingSensors.push_back(pair<Sensor, string>(*sensor, "Le capteur a enregistré des valeurs pour un attribut inconnu !"));
#ifdef STATS
          clog << "Le capteur " << sensor->getSensorID() << " a enregistré des valeurs pour un attribut inconnu ! " << endl;
#endif
        }
      }

      //On check si on a bien tout les attributs
      for (int i = 0; i < nbAtt; i++)
      {
        if (!attValids[i])
        {
          failingSensors.push_back(pair<Sensor, string>(*sensor, "Le capteur n'a pas enregistré de valeurs l'attribut " + this->getAttributesConcerned()[i].getAttributeID()));
#ifdef STATS
          clog << "Le capteur " << sensor->getSensorID() << " n'a pas enregistré de valeurs l'attribut " + this->getAttributesConcerned()[i].getAttributeID() << endl;
#endif
        }
      }
      delete[] attValids;
    }
    else
    {
      failingSensors.push_back(pair<Sensor, string>(*sensor, "Le capteur n'a pas enregistré de valeurs"));
#ifdef STATS
      cout << "Le capteur " << sensor->getSensorID() << " n'a pas enregistré de valeurs." << endl;
#endif
    }
  }

  return failingSensors;
}

vector<pair<Sensor, Sensor>> Statistics::detectSimilarities(const measureMap_t &measureMap, uint timeRange, uint deltaMeasure)
//Algorithme
//
{
#ifdef MAP
  clog << "Appel à la fonction <Statistics::detectSimilarities>" << endl;
#endif
  // Algorithme :
  // Le principe de l'algorithme est simple. Comme indiqué dans le contrat de l'algo, il doit détecter les capteurs similaires.
  // Deux capteurs sont déclarés similaires si toutes leurs mesures sur différentes plage horaires sont
  // proche à un delta = similarityFactor défini plus haut
  //
  // On part du principe que tout les capteurs sont similaires. On stocke ces infos dans la matrice  bool sensorsDifferents[nbSensors][nbSensors];
  // A l'initialisation, on aura
  // sensorsDifferents= x x x
  //                    0 x x
  //                    0 0 x
  // On utilise uniquement la partie inférieure stricte de la matrice (Pour éviter la redondance, question de performance)
  // Puis, si l'on découvre que deux mesures sur une plage donnée sont différentes, on met la valeur à 1.
  // Ici, si les capteur 0 et 1 on une mesure qui diffère de similarityFactor:
  // sensorsDifferents= x x x
  //                    1 x x
  //                    0 0 x
  // Enfin, pour s'assurer de ne pas garantir la similarité si elle n'a pas pu être testé, on rajoute la matrice bool sensorsTested[nbSensors][nbSensors];
  // Au final, on sait que les capteur i et j sont similaires si sensorsDifferents[i][j]=0 et  sensorsTested[i][j]=1

  //Date de départ.
  this->setTemporalFilter(true);
  // On transforme le nombre d'heures en secondes
  timeRange = timeRange  * 60 * 60;
  time_t plage = this->firstMeasure;

  uint nbSensors = this->getSensorsConcerned().size();
  vector<Attribute> attributes = this->getAttributesConcerned();
  bool **sensorsDifferents = new bool *[nbSensors];
  bool **sensorsTested = new bool *[nbSensors];

  //On initialise la matrice pour
  for (uint i = 0; i < nbSensors; i++)
  {
    sensorsDifferents[i] = new bool[nbSensors];
    sensorsTested[i] = new bool[nbSensors];
    for (uint j = 0; j < nbSensors; j++)
    {
      sensorsDifferents[i][j] = false;
      sensorsTested[i][j] = false;
    }
  }
  for (uint i = 0; i < nbSensors; i++)
  {
    sensorsDifferents[i][i] = true;
    sensorsTested[i][i] = true;
  }

#ifdef STATS
  //Affichage de la matrice de similarité
  clog << "Debut " << endl
       << "-------------------" << endl;
  for (uint i = 0; i < nbSensors; i++)
  {
    for (uint j = 0; j < nbSensors; j++)
    {
      clog << " " << (sensorsDifferents[i][j] ? "1" : "0") << " ";
    }
    clog << endl;
  }
  clog << "-------------------" << endl;
#endif

  vector<Attribute> attributeConcerned = this->getAttributesConcerned();
  //On va chercher des similarités pour chaque attribut
  for (vector<Attribute>::iterator atit = attributeConcerned.begin(); atit != attributeConcerned.end(); ++atit)
  {

    this->setAttributesConcerned(vector<Attribute>{*atit});
    //On parcours les heures
    do
    {
      //On change la plage horaire. La plage horaire dure timeRange heures
      this->setBeginTimestamp(plage);
      plage += timeRange;
      this->setEndTimestamp(plage);

      vector<Average> averages = this->calculateAverage(measureMap);
      //On parcourt les sensors 2 à 2
      for (uint i = 0; i < averages.size(); i++)
      {

        for (uint j = 0; j < i; j++)
        {
          //Si on n'a pas déja classé ces sensors comme différents et que la moyenne sur la plage horaire renvoie un nombre
          if (!sensorsDifferents[i][j] && !isnan(averages[i].getValue()) && !isnan(averages[j].getValue()))
          {
            sensorsTested[i][j] = true;                                          // On indique que le sensor a été testé
            double diff = fabs(averages[i].getValue() - averages[j].getValue()); //On calcule la différence entre les deux moyennes
            //Si la différence est trop importante, les sensors sont considérés comme différents
            if (diff > deltaMeasure)
            {
              sensorsDifferents[i][j] = 1; //On indique dans la matrice qu'ils sont différents
            }
          }
        }
      }
    }while(plage < (this->lastMeasure));
#ifdef STATS
    //Affichage de la matrice de similarité
    clog << "Matrice de similarité Attribut:" << atit->getAttributeID() << endl
         << "-------------------" << endl;
    for (uint i = 0; i < nbSensors; i++)
    {
      for (uint j = 0; j < nbSensors; j++)
      {
        clog << " " << (!sensorsDifferents[i][j] && sensorsTested[i][j] ? "1" : "0") << " ";
      }
      clog << endl;
    }
    clog << "-------------------" << endl;
#endif
  }
  //Création des paires
  vector<pair<Sensor, Sensor>> paires;
  for (uint i = 1; i < nbSensors; i++)
  {
    for (uint j = 0; j < i; j++)
    {
      if (!sensorsDifferents[i][j] && sensorsTested[i][j])
      {
        paires.push_back(pair<Sensor, Sensor>(this->getSensorsConcerned()[i], this->getSensorsConcerned()[j]));
      }
    }
  }

  for (uint i = 0; i < nbSensors; i++)
  {
    delete [] sensorsDifferents[i];
    delete [] sensorsTested[i];
  }
  delete [] sensorsDifferents;
  delete [] sensorsTested;
  return paires;
}

void Statistics::clear()
//Algorithme :
//
{
#ifdef MAP
  clog << "Appel à la fonction <Statistics::clear>" << endl;
#endif
  attributesConcerned.clear();
  sensorsConcerned.clear();
}

//-------------------------------------------- Constructeurs - destructeur
Statistics::Statistics()
// Algorithme :
//
{
#ifdef OTRACE
  clog << "Appel au constructeur de <Statistics>" << endl;
#endif
} //----- Fin de Statistics

Statistics::~Statistics()
// Algorithme :
//
{
#ifdef OTRACE
  clog << "Appel au destructeur de <Statistics>" << endl;
#endif
} //----- Fin de ~Statistics

//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées
int Statistics::getFirstDateInRange(vector<Measure> &mes)
{
#ifdef MAP
  clog << "Appel à la fonction <Statistics::getFirstDateInRange>" << endl;
#endif

  // Algorithme: Fonctionne par dichotomie
  int indicemin = -1;
  int indicemax = mes.size();

  time_t beginTimestamp = this->getBeginTimestamp();
  while (indicemax - indicemin > 1)
  {
    int indice = (indicemin + indicemax) / 2;
    if (difftime(mes[indice].GetTimeT(), beginTimestamp) >= 0)
    { // Si Tmesure est supérieur à la date de début
      indicemax = indice;
      //cout << mes[indice].GetTimeT() << " > " << beginTimestamp << "   Imax=" << indicemax << endl;
    }
    else
    {
      indicemin = indice;
      //cout << mes[indice].GetTimeT() << " < " << beginTimestamp << "   Imin=" << indicemin << endl;
    }
  }

  return indicemax;
}

int Statistics::getLastDateInRange(vector<Measure> &mes)
{
#ifdef MAP
  clog << "Appel à la fonction <Statistics::getLastDateInRange>" << endl;
#endif

  // Algorithme: Fonctionne par dichotomie
  int indicemin = -1;
  int indicemax = mes.size();

  time_t endTimestamp = this->getEndTimestamp();
  while (indicemax - indicemin > 1)
  {
    int indice = (indicemin + indicemax) / 2;
    if (difftime(mes[indice].GetTimeT(), endTimestamp) > 0)
    { // Si Tmesure est supérieur à la date de fin
      indicemax = indice;
    }
    else
    {
      indicemin = indice;
    }
  }
  return indicemin;
}
