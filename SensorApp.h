/*************************************************************************
                           SensorApp  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface du module <SensorApp> (fichier SensorApp.h) -------------------
#if !defined(SENSORAPP_H)
#define SENSORAPP_H

//------------------------------------------------------------------------
// Rôle du module <SensorApp>
//
//
//------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////  INCLUDE
//--------------------------------------------------- Interfaces utilisées

#include "Reader.h"
#include <iostream>

using namespace std;
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main();
// Mode d'emploi :
//
// Contrat :
//

void run();
// Mode d'emploi :
//
// Contrat :
//

bool fileExists(const string &fileName);
// Permet de connaitre l'xistance ou non d'un fichier

#endif // SENSORAPP_H
