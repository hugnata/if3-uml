/*************************************************************************
                           Reader  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Reader> (fichier Reader.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Reader.h"
#include "Attribute.h"
//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
pair<time_t, time_t> Reader::loadMeasures(const string &fileName, measureMap_t &datas, const sensorVector_t &sensors, const attributeVector_t &attributes)
// Algorithme :
//
{
#ifdef MAP
    clog << "Appel à la méthode <Reader::loadMeasures>" << endl;
#endif
    open(fileName);
    const unsigned int END = seekg(0, end).tellg();
    seekg(0, beg);

    string sensorID;
    string attributeID;
    Measure m;
    struct tm t;
    t.tm_year = 2017 - 1900;
    t.tm_min = 0;
    t.tm_sec = 0;
    t.tm_isdst = 0;
    t.tm_mon = 0;
    t.tm_mday = 1;
    t.tm_hour = 0;
    t.tm_isdst = 0;
    double value;
    time_t first, last;

    // Suppression des données précédentes
    datas.clear();
#ifdef MAPREADER
    uint cptLigne = 1;
    clog << "ligne en cours de lecture : " << ++cptLigne << endl;
#endif

    // Saut des premières lignes contenant la description dess capteurs
    for (unsigned int i = 0; i < sensors.size() + 2; i++)
    {
        ignore(500, '\n');
    }

    // Lecture et insertion de la première mesure hors de la boucle
    try
    {
        nextMeasureUTF16(t, value, sensorID, attributeID);
    }
    catch (exception const &e)
    {
        cerr << "format fichier invalide, fin d'exécution" << endl;
        exit(EXIT_FAILURE);
    }
    m = Measure(t, value);
    // création du vecteur de mesures
    measureVector_t v;
    v.push_back(m);
    // création de la map attribut/Vecteur de mesures
    attributeMap_t attMap;
    attMap.insert(pair<string, measureVector_t>(attributeID, v));
    // ajout de la map associée à la clé sensorID
    datas.insert(paireSensorAtt_t(sensorID, attMap));

    // Récupération de la date de la première mesure
    first = mktime(&t);
    attributeID.clear();
    sensorID.clear();

    // Boucle de parcours du fichier
    while (tellg() != END && !eof() && is_open())
    {
        // Tentative de lecture de la ligne
        try
        {
#ifdef MAPREADER
            clog << "ligne en cours de lecture : " << ++cptLigne << endl;
#endif
            nextMeasureUTF16(t, value, sensorID, attributeID);
            //cout << "Nouvelle mesure [" << mktime(&t) << "," << value << "," << sensorID << "," << attributeID << "]" << endl;
        }
        catch (exception const &e)
        {
            cerr << "format fichier invalide, fin d'exécution" << endl;
            exit(EXIT_FAILURE);
        }

        // Insertion de la mesure dans la structure de données
        m = Measure(t, value);
        measureMap_t::iterator mIt = datas.find(sensorID);
        if (mIt != datas.end())
        { // Si le sensor est déjà présent dans les données
            attributeMap_t::iterator aMIt = mIt->second.find(attributeID);

            if (aMIt != mIt->second.end())
            { // Si l'attribut associé au Sensor est déjà dans les données

                // insertion de la mesure dans le vecteur de mesures

                aMIt->second.push_back(m);
            }
            else
            { // Si pas l'attribut

                // création du vecteur de mesures
                measureVector_t v;
                v.push_back(m);

                // insertion du vecteur associé à la clé attribut
                mIt->second.insert(pair<string, measureVector_t>(attributeID, v));
            }
        }
        else
        { // Si pas le Sensor

            // création du vecteur de mesures
            measureVector_t v;
            v.push_back(m);

            // création de la map attribut/Vecteur de mesures
            attributeMap_t attMap;
            attMap.insert(pair<string, measureVector_t>(attributeID, v));

            // aitDataout de la map associée à la clé sensorID
            datas.insert(paireSensorAtt_t(sensorID, attMap));
        }

        attributeID.clear();
        sensorID.clear();
    }
    close();

    // Récupération de la date de la dernière mesure
    last = mktime(&t);
    // Si besoin un tri est implémenté ci dessous mais inutilisé car
    // trop gourmand en performances
    /*
    measureMap_t::iterator itData1 = datas.begin();
    measureMap_t::iterator itDataEnd1 = datas.end();
    while (itData1 != itDataEnd1)
    {
        attributeMap_t::iterator itData2 = itData1->second.begin();
        attributeMap_t::iterator itDataEnd2 = itData1->second.end();
        while (itData2 != itDataEnd2)
        {
            measureVector_t measurs = itData2->second;
            sort(measurs.begin(), measurs.end());
            ++itData2;
        }
        ++itData1;
    }
    */

    return make_pair(first, last);
} //----- Fin de loadMeasures

void Reader::loadSensors(const string &fileName, sensorVector_t &sensors)
// Algorithme :
//
{
#ifdef MAP
    clog << "Appel à la méthode <Reader::loadSensors>" << endl;
#endif

    open(fileName);
    const unsigned int END = seekg(0, end).tellg();
    seekg(0, beg);

    std::string sensorID;
    char b1[20];
    double latitude;
    double longitude;
    std::string description;

    // vidage du vecteur de sensors
    sensors.clear();

    // Passer la ligne 1
    ignore(500, '\n');

    // Boucle de parcours du fichier
    while (tellg() != END && !eof() && is_open())
    {
        std::getline(*this, sensorID, ';');
        getline(b1, 20, ';');
        latitude = stod(b1);
        getline(b1, 20, ';');
        longitude = stod(b1);
        std::getline(*this, description, ';');
        ignore(1, '\n');
#ifdef OTRACE
        clog << "sensorID : " << sensorID << endl
             << "lat : " << latitude << endl
             << "long : " << longitude << endl
             << "Description : " << description << endl
             << endl;
#endif
        // Ajout du Sensor dans la structure de données
        Sensor s(sensorID, latitude, longitude, description);
        sensors.push_back(s);
    }
    close();
} //----- Fin de loadSensors

void Reader::loadAttributes(const string &fileName, attributeVector_t &attributes)
// Algorithme :
//
{
#ifdef MAP
    clog << "Appel à la méthode <Reader::loadAttributes>" << endl;
#endif

    open(fileName);
    const unsigned int END = seekg(0, end).tellg();
    seekg(0, beg);

    std::string AttributeID;
    std::string unit;
    std::string description;

    // vidage du vecteur d'attributes
    attributes.clear();

    // Passer la ligne 1
    ignore(500, '\n');

    // Boucle de parcours du fichier
    while (tellg() != END && !eof() && is_open())
    {
        std::getline(*this, AttributeID, ';');
        std::getline(*this, unit, ';');
        std::getline(*this, description, ';');
        ignore(1, '\n');
#ifdef OTRACE
        clog << "AttributeID : " << AttributeID << endl
             << "Unité : " << unit << endl
             << "Description : " << description << endl
             << endl;
#endif

        // Ajout de l'attribut à la structure de données
        Attribute att(AttributeID, unit, description);
        attributes.push_back(att);
    }

    close();
} //----- Fin de loadAttributes

void Reader::nextMeasureUTF16(struct tm &t, double &value, string &sensorID, string &attributeID)
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel à la méthode <Reader::nextMeasureUTF16>" << endl;
#endif
    string b1 = "", b2 = "";

    // Année
    std::getline(*this, b1, '-');
    for (unsigned int i = 1; i < b1.size() - 1; i += 2)
    {
        b2.push_back(b1.at(i));
    }

    if (b2 != "") // Contrôle de fin de fichier non atteinte
    {
        t.tm_year = stoi(b2) - 1900;
        b2.clear();

        // Mois
        std::getline(*this, b1, '-');
        for (unsigned int i = 1; i < b1.size() - 1; i += 2)
        {
            b2.push_back(b1.at(i));
        }
        t.tm_mon = stoi(b2) - 1;
        b2.clear();

        // Jour
        std::getline(*this, b1, 'T');
        for (unsigned int i = 1; i < b1.size() - 1; i += 2)
        {
            b2.push_back(b1.at(i));
        }
        t.tm_mday = stoi(b2);
        b2.clear();

        // Heure
        std::getline(*this, b1, ':');
        for (unsigned int i = 1; i < b1.size() - 1; i += 2)
        {
            b2.push_back(b1.at(i));
        }
        t.tm_hour = stoi(b2);
        b2.clear();

        // Minute
        std::getline(*this, b1, ':');
        for (unsigned int i = 1; i < b1.size() - 1; i += 2)
        {
            b2.push_back(b1.at(i));
        }
        t.tm_min = stoi(b2);
        b2.clear();

        // Secondes
        std::getline(*this, b1, '.');
        for (unsigned int i = 1; i < b1.size() - 1; i += 2)
        {
            b2.push_back(b1.at(i));
        }
        t.tm_sec = stoi(b2);
        b2.clear();

        ignore(100, ';');

        // Sensor ID
        std::getline(*this, b1, ';');
        for (unsigned int i = 1; i < b1.size() - 1; i += 2)
        {
            sensorID.push_back(b1.at(i));
        }

        // Attribut
        std::getline(*this, b1, ';');
        for (unsigned int i = 1; i < b1.size() - 1; i += 2)
        {
            attributeID.push_back(b1.at(i));
        }

        // Valeur
        std::getline(*this, b1, ';');
        for (unsigned int i = 1; i < b1.size() - 1; i += 2)
        {
            b2.push_back(b1.at(i));
        }
        value = stof(b2);
        b2.clear();
    }
    ignore(1000, '\n');
} //----- Fin de nextMeasureUTF16

void Reader::nextMeasureUTF8(struct tm &t, double &value, string &sensorID, string &attributeID)
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel à la méthode <Reader::nextMeasureUTF8>" << endl;
#endif
    string b1 = "";
    t.tm_year = 2017 - 1900;
    t.tm_min = 0;
    t.tm_sec = 0;
    t.tm_isdst = 0;
    t.tm_mon = 0;
    t.tm_mday = 1;
    t.tm_hour = 0;
    t.tm_isdst = 0;

    // Année
    std::getline(*this, b1, '-');

    if (b1 != "") // Contrôle de fin de fichier non atteinte
    {
        t.tm_year = stoi(b1) - 1900;
        b1.clear();

        // Mois
        std::getline(*this, b1, '-');
        t.tm_mon = stoi(b1) - 1;
        b1.clear();

        // Jour
        std::getline(*this, b1, 'T');
        t.tm_mday = stoi(b1);
        b1.clear();

        // Heure
        std::getline(*this, b1, ':');
        t.tm_hour = stoi(b1);
        b1.clear();

        // Minute
        std::getline(*this, b1, ':');
        t.tm_min = stoi(b1);
        b1.clear();

        // Secondes
        std::getline(*this, b1, '.');
        t.tm_sec = stoi(b1);
        b1.clear();

        ignore(100, ';');

        // Sensor ID
        std::getline(*this, sensorID, ';');

        // Attribut
        std::getline(*this, attributeID, ';');

        // Valeur
        std::getline(*this, b1, ';');
        value = stof(b1);
        b1.clear();
    }
    ignore(1000, '\n');
} //----- Fin de nextMeasureUTF8

//-------------------------------------------- Constructeurs - destructeur
Reader::Reader(const Reader &aReader)
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de copie de <Reader>" << endl;
#endif
} //----- Fin de Reader (constructeur de copie)

Reader::Reader()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de <Reader>" << endl;
#endif
} //----- Fin de Reader

Reader::~Reader()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au destructeur de <Reader>" << endl;
#endif
} //----- Fin de ~Reader

//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées
