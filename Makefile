ECHO=@echo
RM=@rm
COMP=g++
EDL=g++
CPPFLAGS=-O3 -Wall -ansi -std=c++11 -pedantic -o0 -DNOMAP  -DNOTRACE -DNSTATS -DNOMAPREADER
EDLFLAGS=
RMFLAGS=-f
EXE=SensorApp
INT=Affichage.cpp Measure.cpp Reader.cpp Sensor.cpp Attribute.cpp Statistics.cpp Average.cpp
REAL=$(INT:.h=.cpp)
OBJ=$(REAL:.cpp=.o) $(EXE).o
TEST=
INCPATH=
LIBPATH=

LIBS=

.PHONY: clean

$(EXE) : $(OBJ)
	$(ECHO) "EDL de $(EXE)"
	$(EDL) $(EDLFLAGS) -o $(EXE) $(OBJ)

%.o : %.cpp
	$(ECHO) "Compilation de $<"
	$(COMP) $(CPPFLAGS) -o $@ -c $<

clean :
	$(RM) $(RMFLAGS) $(OBJ) $(EXE) core *.csv *.gch

test : $(OBJ)
	cd Tests; bash mktest.sh
