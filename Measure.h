/*************************************************************************
                           Measure  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Measure> (fichier Measure.h) ----------------
#if !defined(MEASURE_H)
#define MEASURE_H

//--------------------------------------------------- Interfaces utilisées
#include <ctime>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Measure>
// Objet modélisant une mesure
// caractérisé par une valeur et un horraire de mesure
//------------------------------------------------------------------------

class Measure
{
  //----------------------------------------------------------------- PUBLIC

public:
  //------------------------------------------------- Surcharge d'opérateurs

  friend std::ostream &operator<<(std::ostream &out, const Measure &aMeasure);

  friend bool operator<(const Measure &m1, const Measure &m2);

  Measure &operator=(const Measure &aMeasure);

  //-------------------------------------------------Getters
  //TimeStamp Getter
  struct tm GetTimestamp() const
  {
    return timestamp;
  } //---- Fin

  //TimeStamp Getter
  time_t GetTimeT()
  {
    return mktime(&timestamp);
  } //---- Fin

  //Value Getter
  double GetValue() const
  {
    return value;
  } //---- Fin

  //--------------------------------------------------Setters
  //TimeStamp Setter
  void SetTimestamp(const struct tm &timestamp)
  {
    this->timestamp = timestamp;
  } //---- Fin

  //Value setter
  void SetValue(const double &value)
  {
    this->value = value;
  } //---- Fin

  //-------------------------------------------- Constructeurs - destructeur
  Measure(const Measure &aMeasure);
  // Mode d'emploi (constructeur de copie) :
  //
  // Contrat :
  //

  Measure(const struct tm &timestamp, const double &value);
  // Mode d'emploi :
  //
  // Contrat :
  //

  Measure();
  // Mode d'emploi :
  //
  // Contrat :
  //

  virtual ~Measure();
  // Mode d'emploi :
  //
  // Contrat :
  //

  //------------------------------------------------------------------ PRIVE

protected:
  //----------------------------------------------------- Attributs protégés

  struct tm timestamp;
  double value;
};

//-------------------------------- Autres définitions dépendantes de <Measure>

#endif // MEASURE_H
