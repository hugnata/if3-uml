/*************************************************************************
                           Average  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Average> (fichier Average.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Average.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//--------------------------------------------------Surcherge d'opérateurs
Average &Average::operator=(const Average &anAverage)
// Algorithme
{
#ifdef MAP
  clog << "Appel à l'opérateur = de <Average>" << endl;
#endif
  this->attribute = anAverage.attribute;
  this->sensor = anAverage.sensor;
  this->value = anAverage.value;
  return *this;
}

//-------------------------------------------- Constructeurs - destructeur
Average::Average()
// Algorithme :
//
{
#ifdef OTRACE
  clog << "Appel au constructeur par défaut de <Average>" << endl;
#endif
} //----- Fin de Average

Average::Average(const Average &anAverage)
// Algorithme :
//
{
#ifdef OTRACE
  clog << "Appel au constructeur de <Average>" << endl;
#endif
  this->attribute = anAverage.attribute;
  this->sensor = anAverage.sensor;
  this->value = anAverage.value;
} //----- Fin de Average

Average::Average(const Sensor &sensor, const Attribute &attribute, long double &value)
// Algorithme :
//
{
#ifdef OTRACE
  clog << "Appel au constructeur de <Average>" << endl;
#endif
  this->sensor = sensor;
  this->attribute = attribute;
  this->value = value;

} //----- Fin de Average

Average::~Average()
// Algorithme :
//
{
#ifdef OTRACE
  clog << "Appel au destructeur de <Average>" << endl;
#endif
} //----- Fin de ~Average

//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées
