/*************************************************************************
                           Measure  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Measure> (fichier Measure.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Measure.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//------------------------------------------------- Surcharge d'opérateurs

std::ostream &operator<<(std::ostream &out, const Measure &aMeasure)
// Algorithme :
//
{
#ifdef MAP
    clog << "Appel à la fonction < operator<< > pour l'objet Measure" << endl;
#endif

    out << "horaire de mesure : " << aMeasure.timestamp.tm_mday << "/" << aMeasure.timestamp.tm_mon << "/" << aMeasure.timestamp.tm_year << " - " << aMeasure.timestamp.tm_hour << ":" << aMeasure.timestamp.tm_min << ":" << aMeasure.timestamp.tm_sec << "\t\t"
        << "valeur : " << aMeasure.value;
    return out;
} //------ Fin

Measure &Measure::operator=(const Measure &aMeasure)
// Algorithme :
//
{
#ifdef MAP
    clog << "Appel à la fonction < operator= > pour l'objet Measure" << endl;
#endif
    value = aMeasure.value;
    timestamp = aMeasure.timestamp;
    return *this;
} //------ Fin

bool operator<(const Measure &m1, const Measure &m2)
// Algorithme :
//
{
#ifdef MAP
    clog << "Appel à la fonction < operator< > pour l'objet Measure" << endl;
#endif
    struct tm t1 = m1.timestamp, t2 = m2.timestamp;
    return mktime(&t1) < mktime(&t2);
} //------ Fin

//-------------------------------------------- Constructeurs - destructeur
Measure::Measure(const Measure &aMeasure)
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de copie de <Measure>" << endl;
#endif

    value = aMeasure.value;
    timestamp = aMeasure.timestamp;
} //----- Fin de Measure (constructeur de copie)

Measure::Measure(const struct tm &timestamp, const double &value)
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de <Measure>" << endl;
#endif
    this->timestamp = timestamp;
    this->value = value;
} //----- Fin de Measure

Measure::Measure()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de <Measure>" << endl;
#endif
} //----- Fin de Measure

Measure::~Measure()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au destructeur de <Measure>" << endl;
#endif
} //----- Fin de ~Measure

//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées
