/*************************************************************************
                           Average  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Average> (fichier Average.h) ----------------
#if !defined(AVERAGE_H)
#define AVERAGE_H

//--------------------------------------------------- Interfaces utilisées
#include "Sensor.h"
#include "Attribute.h"
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Average>
//
//
//------------------------------------------------------------------------

class Average
{
  //----------------------------------------------------------------- PUBLIC

public:
  //------------------------------------------------- Surcharge d'opérateurs
  Average &operator=(const Average &anAverage);
  // Mode d'emploi :
  //
  // Algorithme :
  //

  //-------------------------------------------- Constructeurs - destructeur
  Average(const Average &anAverage);
  // Mode d'emploi (constructeur de copie) :
  //
  // Contrat :
  //

  Average();
  // Mode d'emploi :
  //
  // Contrat :
  //

  Average(const Sensor &sensor, const Attribute &attribute, long double &value);
  // Mode d'emploi :
  //
  // Contrat :
  //

  virtual ~Average();
  // Mode d'emploi :
  //
  // Contrat :
  //

  //------------------------------------------------------------ Accesseurs
  void setSensor(const Sensor &sensor)
  {
    this->sensor = sensor;
  } //-----Fin

  Sensor getSensor() const
  {
    return this->sensor;
  } //-----Fin

  void setAttribute(const Attribute &attribute)
  {
    this->attribute = attribute;
  } //-----Fin

  Attribute getAttribute() const
  {
    return this->attribute;
  } //-----Fin

  void setValue(const double &value)
  {
    this->value = value;
  } //-----Fin

  double getValue() const
  {
    return this->value;
  } //-----Fin

  //------------------------------------------------------------------ PRIVE

protected:
  //----------------------------------------------------- Attributs protégés

  Sensor sensor;
  Attribute attribute;
  long double value;
};

//-------------------------------- Autres définitions dépendantes de <Average>

#endif // AVERAGE_H
