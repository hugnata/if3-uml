/*************************************************************************
                           Attribute  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Attribute> (fichier Attribute.h) ----------------
#if !defined(ATTRIBUTE_H)
#define ATTRIBUTE_H

//--------------------------------------------------- Interfaces utilisées
#include <string>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Attribute>
// Modélise l'attibut d'une mesure
// Caractérisé par son ID, son unité et sa description
//
//------------------------------------------------------------------------

class Attribute
{
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques

    //---------------------------------------------------------------- Getters

    std::string getAttributeID() const
    {
        return id;
    } // ------- fin

    //-------------------------------------------- Constructeurs - destructeur

    Attribute();
    // Mode d'emploi :
    //
    // Contrat :
    //

    Attribute(const Attribute &unAttribute);
    // Mode d'emploi :
    //
    // Contrat :
    //

    Attribute(const std::string &ID, const std::string &UNIT, const std::string &DESCRIPTION);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Attribute();
    // Mode d'emploi :
    //
    // Contrat :
    //

    //------------------------------------------------------------------ PRIVE

protected:
    //----------------------------------------------------- Attributs protégés

    std::string id;
    std::string unit;
    std::string description;
};

//-------------------------------- Autres définitions dépendantes de <Attribute>

#endif // ATTRIBUTE_H
