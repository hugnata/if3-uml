/*************************************************************************
                           Affichage  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par M.WOIRIN - T.LIU - H.RAYMOND - M.MESSALTI
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Affichage> (fichier Affichage.h) ----------------
#if !defined(AFFICHAGE_H)
#define AFFICHAGE_H

//--------------------------------------------------- Interfaces utilisées
#include <string>
#include <vector>
#include "Attribute.h"
#include "Sensor.h"
#include "Measure.h"
#include "Reader.h"
#include "Statistics.h"
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Affichage>
//
// Classe servant à l'affichage des menus de l'application
//
//------------------------------------------------------------------------

class Affichage
{
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques
    void mainMenu();
    // Mode d'emploi :
    // Menu Principal
    //
    //

    void detectFailingSensorMenu();
    // Mode d'emploi :
    // Menu détection de capteur défaillant
    //
    //

    void statsMenu();
    // Mode d'emploi :
    // Menu de sélection des options pour les statistiques
    //
    //

    void timeFilter();
    // Mode d'emploi :
    // Menu pour le filtrage temporel
    //
    //

    void spaceFilter();
    // Mode d'emploi :
    // Menu pour le filtrage spatial
    //
    //

    void attributeChoice();
    // Mode d'emploi :
    //  Menu pour le choix de l'attribut
    //
    //

    void sensorChoice();
    // Mode d'emploi :
    // Menu pour le choix du capteur
    //
    //

    void loadCSV();
    // Mode d'emploi :
    // Menu de chargement des fichiers CSV
    //
    //

    void printAvereage();
    // Mode d'emploi :
    // Affiche le résultat des statistiques
    //
    //

    void printSimilarities();
    // Mode d'emploi :
    // Afiiche le résultat de la recherche de similarités
    //
    //

    //-------------------------------------------- Constructeurs - destructeur

    Affichage();
    //
    //
    //
    //

    virtual ~Affichage();
    //
    //
    //
    //

    //------------------------------------------------------------------ PRIVE

protected:
    //----------------------------------------------------- Attributs protégés

    measureMap_t measures;
    sensorVector_t sensors;
    attributeVector_t attributes;
    Statistics stats;
};

//-------------------------------- Autres définitions dépendantes de <Affichage>

#endif // AFFICHAGE_H
