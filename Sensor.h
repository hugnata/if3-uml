/*************************************************************************
                           Sensor  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Interface de la classe <Sensor> (fichier Sensor.h) ----------------
#if !defined(SENSOR_H)
#define SENSOR_H

//--------------------------------------------------- Interfaces utilisées
#include <string>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Sensor>
//
//
//------------------------------------------------------------------------

class Sensor
{
  //----------------------------------------------------------------- PUBLIC

public:
  //----------------------------------------------------- Méthodes publiques
  bool containedInCircle(double latitudeCentre, double longitudeCentre, double radius) const;
  // Mode d'emploi :
  //
  // Contrat :
  //

  //------------------------------------------------Getters
  std::string getSensorID() const
  {
    return this->sensorID;
  } //------ Fin

  double getLatitude() const
  {
    return latitude;
  } //------ Fin

  double getLongitude() const
  {
    return longitude;
  } //------ Fin

  std::string getDescription() const
  {
    return description;
  } //------ Fin

  //------------------------------------------------Setters
  void setSensorID(const std::string &sensorID)
  {
    this->sensorID = sensorID;
  } //------ Fin

  void setLatitude(const double &latitude)
  {
    this->latitude = latitude;
  } //------ Fin

  void setLongitude(const double &longitude)
  {
    this->longitude = longitude;
  } //------ Fin

  void setDescription(const std::string &description)
  {
    this->description = description;
  } //------ Fin

  //-------------------------------------------- Constructeurs - destructeur
  Sensor();
  // Mode d'emploi :
  //
  // Contrat :
  //

  Sensor(const Sensor &unSensor);
  // Mode d'emploi :
  //
  // Contrat :
  //

  Sensor(const std::string &ID, const double &LATITUDE, const double &LONGITUDE, const std::string &DESCRIPTION);
  // Mode d'emploi :
  //
  // Contrat :
  //

  virtual ~Sensor();
  // Mode d'emploi :
  //
  // Contrat :
  //

  //------------------------------------------------------------------ PRIVE

protected:
  //----------------------------------------------------- Attributs protégés
  std::string sensorID;
  double latitude;
  double longitude;
  std::string description;
};

//-------------------------------- Autres définitions dépendantes de <Sensor>

#endif // SENSOR_H
