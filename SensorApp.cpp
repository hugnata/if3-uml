/*************************************************************************
                           SensorApp  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <SensorApp> (fichier SensorApp.cpp) ---------------

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "SensorApp.h"
#include "Statistics.h"
#include "Affichage.h"
#include <vector>
///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

bool fileExists(const string &fileName)
// Permet de connaitre l'xistance ou non d'un fichier
{
#ifdef MAP
  clog << "Appel de la fonction <fileExists> de <SensorApp>" << endl;
#endif
  ifstream file(fileName);
  return file.good();
}
//----- Fin de FileExists

void run()
{
#ifdef MAP
  clog << "Début de l'exécution" << endl;
#endif

  Affichage disp;

  // Premier chargement  des données
  disp.loadCSV();
  // Lancement du menu principal
  disp.mainMenu();

#ifdef MAP
  clog << "exécution terminée" << endl;
#endif
}

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main()
{
#ifdef MAP
  ofstream debugFile("Debug.txt");
  streambuf *oldClog = clog.rdbuf(debugFile.rdbuf());
#endif

  run();

#ifdef MAP
  clog.rdbuf(oldClog);
#endif
  return EXIT_SUCCESS;
}
