/*************************************************************************
                           Affichage  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par M.WOIRIN - T.LIU - H.RAYMOND - M.MESSALTI
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Affichage> (file Affichage.cpp) ------------
#pragma GCC diagnostic ignored "-Wunused-result"
//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include <math.h>
using namespace std;
//------------------------------------------------------ Include personnel
#include "Affichage.h"
#include "Sensor.h"
#include "Attribute.h"
#include "SensorApp.h"
#include "Statistics.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
void Affichage::statsMenu()
// Algorithme :
// bouble switch pour exécutant différentes actions selon la saisie
//
{
#ifdef MAP
    clog << "Appel à la fonction <Affichage::statsMenu>" << endl;
#endif
    system("clear");
    stats.clear();

    string buffer;
    uint n = 0;

    while (n != 5)
    {
        cout << ".:Statistiques:.\n"
             << endl;
        cout << "Que faire?" << endl;

        cout << "  1:Sélection par capteur" << endl;
        cout << "  2:Sélection par zone" << endl;
        cout << "  3:Global" << endl;
        cout << "  4:Détection de similaritées" << endl;
        cout << "  5:Retour" << endl;

        cout << "\nSaisissez un nombre : ";
        cin >> buffer;

        // Vérification du bon format d'entrée
        try
        {
            n = stoi(buffer);
        }
        catch (const std::exception &e)
        {
            n = 15;
        }

        cout << endl;

        switch (n)
        {
        case 1: // Sélection par capteur
            sensorChoice();
            n = 5;
            break;
        case 2: // Sélection par zone
            spaceFilter();
            n = 5;
            break;
        case 3: // Global
            stats.setSensorsConcerned(sensors);
            attributeChoice();
            n = 5;
            break;
        case 4: // Détection de similarités
            printSimilarities();
            break;
        case 5: // Retour
            break;
        default: // Saisie invalide
            cout << "Saisie incorrecte" << endl;
        }
    }

} //----- Fin de Méthode

void Affichage::mainMenu()
// Algorithme :
// bouble switch pour exécutant différentes actions selon la saisie
//
{
#ifdef MAP
    clog << "Appel à la fonction <Affichage::mainMenu>" << endl;
#endif

    system("clear");
    string buffer;
    int n = 0;

    while (n != 4)
    {
        cout << ".:Menu principal:.\n"
             << endl;
        cout << "Que faire?" << endl;

        cout << "  1:Charger nouveaux CSV" << endl;
        cout << "  2:Menu Statistiques" << endl;
        cout << "  3:Recherche de dysfonctionnements" << endl;
        cout << "  4:Quitter" << endl;

        cout << "\nSaisissez un nombre : ";
        cin >> buffer;

        // Vérification sur la saisie
        try
        {
            n = stoi(buffer);
        }
        catch (const std::exception &e)
        {
            n = -1;
        }

        cout << endl;

        switch (n)
        {
        case 1: // Charger nouveaux CSV
            loadCSV();
            break;
        case 2: // Menu Statistiques
            statsMenu();
            break;
        case 3: // Recherche de Dysfonctionnements
            detectFailingSensorMenu();
            break;
        case 4: // Quitter
            break;
        default: // Erreurs de saisie
            cout << "Saisie incorrecte" << endl;
        }
    }
}
void Affichage::timeFilter()
// Algorithme :
//
//
{
#ifdef MAP
    clog << "Appel à la fonction <Affichage::timeFilter>" << endl;
#endif

    system("clear");

    // Affichage des choix déjà faits
    sensorVector_t::const_iterator end1 = stats.getSensorsConcerned().cend();
    cout << "Capteur(s) sélectionné(s) : ";
    for (sensorVector_t::const_iterator i = stats.getSensorsConcerned().cbegin(); i != end1; i++)
    {
        cout << i->getSensorID() << "; ";
    }
    cout << endl;
    cout << "Attributs sélectionné(s) : ";
    attributeVector_t::const_iterator end2 = stats.getAttributesConcerned().cend();
    for (attributeVector_t::const_iterator i = stats.getAttributesConcerned().cbegin(); i != end2; i++)
    {
        cout << i->getAttributeID() << "; ";
    }
    cout << "\n"
         << endl;

    bool v = false;
    int n = 0; // option
    string h, d, m, y, buffer;
    struct tm t;
    t.tm_year = 2017 - 1900;
    t.tm_min = 0;
    t.tm_sec = 0;
    t.tm_isdst = 0;
    t.tm_mon = 0;
    t.tm_mday = 1;
    t.tm_hour = 0;

    cout << ".:Filtrage temporel:.\n"
         << endl;
    cout << "Voulez vous effectuer un filtrage temporel des valeurs ?" << endl;

    while (n != 3)
    {
        cout << "  1:Oui" << endl;
        cout << "  2:Non" << endl;
        cout << "  3:Retour" << endl;

        cout << "\nSaisissez un nombre : ";
        cin >> buffer;

        // Détection d'erreurs de saisie
        try
        {
            n = stoi(buffer);
        }
        catch (const std::exception &e)
        {
            n = -1;
        }

        switch (n)
        {
        case 1: // Oui
            stats.setTemporalFilter(true);

            // Saisie heure début
            while (!v)
            {
                v = true;
                cout << "Date de debut (Saisir 00/00/00/0000 si on ne veut pas en imposer) (format hh/jj/mm/aaaa)" << endl;
                getline(cin, h, '/');
                getline(cin, d, '/');
                getline(cin, m, '/');
                getline(cin, y);
                try
                {
                    t.tm_mon = stoi(m);
                    t.tm_mday = stoi(d);
                    t.tm_hour = stoi(h);
                    t.tm_year = stoi(y);
                    // Test si la date est à un format correct
                    if (t.tm_mon < 0 ||
                        t.tm_mon > 12 ||
                        t.tm_mday < 0 ||
                        t.tm_mday > 31 ||
                        t.tm_hour < 0 ||
                        t.tm_hour > 24 ||
                        t.tm_year < 0 ||
                        t.tm_year > 2019 ||
                        (t.tm_mon != 0 && (t.tm_hour == 0 || t.tm_mday == 0 || t.tm_year == 0)) ||
                        (t.tm_year != 0 && (t.tm_hour == 0 || t.tm_mday == 0 || t.tm_mon == 0)))
                    {
                        throw domain_error("Temps invalide");
                    }
                }
                catch (exception const &e)
                {
                    cerr << "Saisie invalide" << endl;
                    v = false;
                }
            }
            if (t.tm_year < 1900)
            {
                t.tm_year = 1900;
            }
            // Si 00/00/00/0000
            if (t.tm_mon == 0 && t.tm_mday == 0 && t.tm_hour == 0 && t.tm_year == 0)
            {
                stats.setBeginTimestamp();
            }
            else
            {
                --t.tm_mon;
                t.tm_year -= 1900;
            }
            stats.setBeginTimestamp(t);
            v = false;

            // Saisie heure fin
            while (!v)
            {
                v = true;
                cout << "Date de fin (Saisir 00/00/00/0000 si on ne veut pas en imposer) (format hh/jj/mm/aaaa)" << endl;
                getline(cin, h, '/');
                getline(cin, d, '/');
                getline(cin, m, '/');
                getline(cin, y);
                try
                {
                    t.tm_mon = stoi(m);
                    t.tm_mday = stoi(d);
                    t.tm_hour = stoi(h);
                    t.tm_year = stoi(y);
                    // Test si la date est au foramt correct
                    if (t.tm_mon < 0 ||
                        t.tm_mon > 12 ||
                        t.tm_mday < 0 ||
                        t.tm_mday > 31 ||
                        t.tm_hour < 0 ||
                        t.tm_hour > 24 ||
                        t.tm_year < 0 ||
                        t.tm_year > 2019 ||
                        (t.tm_mon != 0 && (t.tm_hour == 0 || t.tm_mday == 0 || t.tm_year == 0)) ||
                        (t.tm_year != 0 && (t.tm_hour == 0 || t.tm_mday == 0 || t.tm_mon == 0)))
                    {
                        throw domain_error("Temps invalide");
                    }
                }
                catch (exception const &e)
                {
                    cerr << "Saisie invalide" << endl;
                    v = false;
                }
            }
            if (t.tm_year < 1900)
            {
                t.tm_year = 1900;
            }
            // Si 00/00/00/0000
            if (t.tm_mon == 0 && t.tm_mday == 0 && t.tm_hour == 0 && t.tm_year == 0)
            {
                stats.setEndTimestamp();
            }
            else
            {
                --t.tm_mon;
                t.tm_year -= 1900;
            }
            stats.setEndTimestamp(t);
            printAvereage();
            n = 3;

            break;
        case 2: // Non
            stats.setTemporalFilter(false);
            printAvereage();
            n = 3;
            break;
        case 3: // Retour
            break;
        default: // Erreurs de saisie
            cout << "Saisie incorrecte" << endl;
        }
    }
} //----- Fin de Méthode

void Affichage::spaceFilter()
// Algorithme :
//
//
{
#ifdef MAP
    clog << "Appel à la fonction <Affichage::spaceFilter>" << endl;
#endif

    system("clear");
    bool v = false;
    int lat, longi, radius;
    string s;

    // Coordonées du centre du cercle de sélection
    while (!v)
    {
        v = true;
        cout << ".:Filtrage Spatial:.\n"
             << endl;
        cout << "Saisir les coordonnées du centre de la zone de sélection (format lat:long) : " << endl;

        // Détection des erreurs de saisie
        try
        {
            getline(cin, s, ':');
            lat = stod(s);
            getline(cin, s);
            longi = stod(s);
        }
        catch (exception const &e)
        {
            cerr << "Saisie invalide" << endl;
            v = false;
        }
    }
    v = false;

    // rayon du cercle de sélection
    while (!v)
    {
        cout << "Saisir le rayon de la zone de sélection en km(>0) : ";
        cin >> s;
        // Détection des erreurs de saisie
        try
        {
            v = true;
            radius = stoi(s);
        }
        catch (const std::exception &e)
        {
            radius = -1;
            v = false;
        }
    }
#ifdef MAP
    clog << lat << "  " << longi << "  " << radius << endl;
#endif

    // Recherche des capteurs dans la zone sélectionnée
    sensorVector_t sensorsInCircle;
    sensorVector_t::const_iterator sensorIt = sensors.cbegin();
    while (sensorIt != sensors.cend())
    {
        if (sensorIt->containedInCircle(lat, longi, radius))
        {
            sensorsInCircle.push_back(*sensorIt);
        }
        ++sensorIt;
    }

    if (sensorsInCircle.cbegin() != sensorsInCircle.cend())
    {
        stats.setSensorsConcerned(sensorsInCircle);

        attributeChoice();
    }
    else
    {
        cout << "Aucun capteur dans cette zone" << endl;
    }

} //----- Fin de Méthode

void Affichage::attributeChoice()
// Algorithme :
// bouble switch pour exécutant différentes actions selon la saisie
//
{
#ifdef MAP
    clog << "Appel à la fonction <Affichage::attributeChoice>" << endl;
#endif

    system("clear");

    //Affichege des choix déjà réalisés
    cout << "Capteur(s) sélectionné(s) : ";
    sensorVector_t::const_iterator end = stats.getSensorsConcerned().cend();
    for (sensorVector_t::const_iterator i = stats.getSensorsConcerned().cbegin(); i != end; i++)
    {
        cout << i->getSensorID() << "; ";
    }
    cout << "\n"
         << endl;

    string buffer;
    uint n = 0;
    while (n != attributes.size() + 2)
    {
        cout << ".:Sélection de l'attribut:.\n"
             << endl;
        cout << "Quel attribut souhaitez-vous mesurer?" << endl;
        for (n = 1; n < attributes.size() + 1; n++)
        {
            cout << "  " << n << "." << attributes[n - 1].getAttributeID() << endl;
        }
        cout << "  " << n << ".Tous" << endl;
        cout << "  " << ++n << ".Retour" << endl;
        cout << "\nSaississez un nombre : ";
        cin >> buffer;

        // Vérification que la saisie est au bon format
        try
        {
            n = stoi(buffer);
        }
        catch (const std::exception &e)
        {
            n = -1;
        }

        cout << endl;
        if (n < 0 || n > attributes.size() + 2) // Saisie invalide
        {
            cout << "Saisie incorrecte" << endl;
        }
        else if (n == attributes.size() + 1) // Tous
        {
            stats.setAttributesConcerned(attributes);
            timeFilter();
            n = attributes.size() + 2;
        }
        else if (n < attributes.size() + 1) // Choix d'un attribut
        {
            stats.addAttributeConcerned(attributes[n - 1]);
            timeFilter();
            n = attributes.size() + 2;
        }
    }
} //----- Fin de Méthode

void Affichage::sensorChoice()
// Algorithme :
// bouble switch pour exécutant différentes actions selon la saisie
//
{
#ifdef MAP
    clog << "Appel à la fonction <Affichage::sensorChoice>" << endl;
#endif

    system("clear");
    string buffer;
    uint n = 0;
    while (n != sensors.size() + 1)
    {
        cout << ".:Selection d'un capteur:.\n"
             << endl;
        cout << "Quel capteur étudier ?" << endl;

        for (n = 1; n < sensors.size() + 1; n++)
        {
            cout << "  " << n << "." << sensors[n - 1].getSensorID() << endl;
        }
        cout << "  " << n << ".Retour" << endl;

        cout << "\nSaississez un nombre : ";
        cin >> buffer;
        // Vérification de la validité de la saisie
        try
        {
            n = stoi(buffer);
        }
        catch (const std::exception &e)
        {
            n = -1;
        }
        cout << endl;
        if (n < 0 || n > sensors.size() + 1) // Saisie incorrecte
        {
            cout << "Saisie incorrecte" << endl;
        }
        else if (n < sensors.size() + 1) // Choix d'un capteur en particulier
        {
            stats.addSensorConcerned(sensors[n - 1]);
            attributeChoice();
            n = sensors.size() + 1;
        }
    }
} //----- Fin de Méthode

void Affichage::loadCSV()
// Algorithme :
//
{
#ifdef MAP
    clog << "Appel à la fonction <Affichage::loadCSV>" << endl;
#endif

    system("clear");
    string fileMeasure, fileSensor, fileAttribute;
    Reader r;
#ifndef MAP
    bool filesExist = false;
    while (!filesExist)
    {
        cout << ".:Charger un fichier CSV:.\n"
             << endl;
        cout << "Veuillez saisir le nom du fichier qui contient les mesures :" << endl;
        cin >> fileMeasure;

        cout << "\nVeuillez saisir le nom du fichier qui contient la description des capteurs :" << endl;
        cin >> fileSensor;

        cout << "\nVeuillez saisir le nom du fichier qui contient la description des attributs :" << endl;
        cin >> fileAttribute;
        cout << endl;
        filesExist = true;
        if (!fileExists(fileMeasure))
        {
            cerr << "Le fichier : " << fileMeasure << " n'existe pas, nouvelle saise." << endl;
            filesExist = false;
        }
        else if (!fileExists(fileSensor))
        {
            cerr << "Le fichier : " << fileSensor << " n'existe pas, nouvelle saise." << endl;
            filesExist = false;
        }
        else if (!fileExists(fileAttribute))
        {
            cerr << "Le fichier : " << fileAttribute << " n'existe pas, nouvelle saise." << endl;
            filesExist = false;
        }
    }
#endif
#ifdef MAP // Chargement par défaut en mode Debug
    //fileMeasure = "../data_10sensors_1year.csv";
    fileMeasure = "Tests/Data/similarite.csv";
    fileSensor = "Tests/Data/sensors.csv";
    fileAttribute = "Tests/Data/AttributeType.csv";
#endif
    cout << "Saisie validée." << endl;

    cout << "Chargement des valeurs en cours..." << endl;
    r.loadSensors(fileSensor, sensors);
    r.loadAttributes(fileAttribute, attributes);
    pair<time_t, time_t> range = r.loadMeasures(fileMeasure, measures, sensors, attributes);
    stats.setMeasureRange(range);
#ifdef MAP
    clog << "première mesure : " << stats.getFirstMeasure() << endl
         << "dernière mesure : " << stats.getLastMeasure() << endl;
#endif
    cout << "Chargement terminé." << endl;

} //----- Fin de Méthode

void Affichage::printSimilarities()
{
#ifdef MAP
    clog << "Appel à la fonction <Affichage::printSimilarites>" << endl;
#endif
    //Initialisation de la détection
    system("clear");
    stats.setSensorsConcerned(sensors);
    stats.resetAttributesConcerned();
    stats.addAttributeConcerned(attributes[0]);
    vector<pair<Sensor, Sensor>> paires;
    cout << "Comment fonctionne la détection de similarités ? " << endl;
    cout << "--------------------------------------------------" << endl;
    cout << "Prenons des capteurs c1 et c2. " << endl;
    cout << "On prendra pour une durée T en heure, les moyennes m1 et m2 des mesures effectuées dans l'intervalle [t;t+T]." << endl;
    cout << "On refait cette opération toute les T heures (t=t+T)" << endl;
    cout << "Seuls les capteurs dont la différence |m1-m2| est supérieure à Delta sur toute la plage horaire seront affichés" << endl;
    cout << "Vous pouvez ainsi déterminer les capteurs similaires avec la granularité voulue" << endl;
    cout << "--------------------------------------------------" << endl;
    cout << "Appuyez sur entrée pour continuer" << endl;
    cin.ignore();
    cin.get();

    //Taille de la plage
    cout << "Saisissez la taille des plages horaire étudiées (en h) : ";
    string buffer;
    cin >> buffer;
    int tailleplage = 0;
    try
    {
        tailleplage = stoi(buffer);
        if (tailleplage < 1)
        {
            cout << "Format de plage invalide ! " << endl;
        }
    }
    catch (const std::exception &e)
    {
        cout << "Format de plage invalide ! " << endl;
        return;
    }

    //Facteur de similarité
    cout << "Saisissez le Delta de mesure maximal pour que deux mesures soient considérées comme similaires: ";
    cin >> buffer;
    int deltaMesure = 0;
    try
    {
        deltaMesure = stoi(buffer);
        if (deltaMesure < 1)
        {
            throw domain_error("Delta trop petit");
        }
    }
    catch (const std::exception &e)
    {
        cout << "Delta invalide ! " << endl;
        return;
    }

    //Détection
    paires = stats.detectSimilarities(measures, tailleplage, deltaMesure);
    cout << "La détection de similarités a trouvé " << paires.size() << " paires de capteurs similaires" << endl;
    cout << "-------------------------" << endl;
    for (uint i = 0; i < paires.size(); i++)
    {
        cout << paires[i].first.getSensorID() << " et " << paires[i].second.getSensorID() << endl;
    }
    cout << "-------------------------" << endl;
    cout << "Appuyez sur entrée pour continuer" << endl;
    cin.ignore();
    cin.get();
    system("clear");
}
void Affichage::printAvereage()
{
#ifdef MAP
    clog << "Appel à la fonction printAvereage()" << endl;
#endif
    system("clear");
    cout << "Calcul de la moyenne en cours..." << endl;

    // Affichage des sélections effectuées
    cout << "Capteur(s) sélectionné(s) : ";
    sensorVector_t::const_iterator end1 = stats.getSensorsConcerned().cend();
    for (sensorVector_t::const_iterator i = stats.getSensorsConcerned().cbegin(); i != end1; i++)
    {
        cout << i->getSensorID() << "; ";
    }
    cout << endl;
    cout << "Attributs sélectionnés : ";
    attributeVector_t::const_iterator end2 = stats.getAttributesConcerned().cend();
    for (attributeVector_t::const_iterator i = stats.getAttributesConcerned().cbegin(); i != end2; i++)
    {
        cout << i->getAttributeID() << "; ";
    }
    cout << "\n"
         << endl;
    if (stats.getTemporalFilter())
    {
        cout << "Plage horaire pour les valeurs : " << stats.getBeginTimestamp() << " - " << stats.getEndTimestamp() << endl;
    }

    vector<Average> averages = stats.calculateAverage(measures);
    cout << "Resultats: " << endl;
    cout << "-------------------------" << endl;
    for (uint i = 0; i < averages.size(); i++)
    {
        cout << averages[i].getSensor().getSensorID() << " - " << averages[i].getAttribute().getAttributeID() << ": ";
        if (isnan(averages[i].getValue()))
        {
            cout << "Impossible à déterminer" << endl;
        }
        else
        {
            cout << averages[i].getValue() << endl;
        }
    }
    cout << "-------------------------" << endl;
    cout << "Appuyez sur entrée pour continuer" << endl;
    cin.ignore();
    cin.get();
    system("clear");
}

void Affichage::detectFailingSensorMenu()
{
#ifdef MAP
    clog << "Appel à la fonction detectFailingSensorMenu()" << endl;
#endif
    system("clear");
    cout << "Détection des capteurs défaillants" << endl;
    cout << "Veuillez entrer la fréquence (en minute) d'échantillonnage des capteurs" << endl;
    int deltaT = 0;
    string buffer;
    cin >> buffer;

    // Vérification qu'un entier est bien saisi
    try
    {
        deltaT = stoi(buffer);
    }
    catch (const std::exception &e)
    {
        deltaT = 0;
    }

    if (deltaT < 1)
    {
        cout << "Le delta T est invalide " << endl;
        cout << "Appuyez sur entrée pour continuer" << endl;
        cin.ignore();
        cin.get();
        system("clear");
    }

    stats.setAttributesConcerned(attributes);
    stats.setSensorsConcerned(sensors);
    cout << "Calcul en cours..." << endl;
    vector<pair<Sensor, string>> vecteursDefectueux = stats.detectFailingSensors(measures, deltaT);
    cout << "Les capteurs défaillants sont les suivants:" << endl;
    cout << "----------------------------------" << endl;

    //Pour la mise en forme, on affiche le nom du sensor qu'une fois
    string lastSensorId = "";
    for (vector<pair<Sensor, string>>::iterator it = vecteursDefectueux.begin(); it != vecteursDefectueux.end(); it++)
    {
        if (lastSensorId.compare(it->first.getSensorID()) == 0)
        {
            cout << "\t- " << it->second << endl;
        }
        else
        {
            lastSensorId = it->first.getSensorID();
            cout << it->first.getSensorID() << ": " << endl;
            cout << "\t- " << it->second << endl;
        }
    }
    cout << "----------------------------------" << endl;
    cout << "Appuyez sur entrée pour continuer" << endl;
    stats.clear();
    cin.ignore();
    cin.get();
}

//-------------------------------------------- Constructeurs - destructeur

Affichage::Affichage()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de <Affichage>" << endl;
#endif
} //----- Fin de Affichage

Affichage::~Affichage()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au destructeur de <Affichage>" << endl;
#endif
} //----- Fin de ~Affichage

//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées
