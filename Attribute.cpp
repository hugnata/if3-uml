/*************************************************************************
                           Attribute  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//----------- Réalisation de la classe <Attribute> (fichier Attribute.cpp)

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <string>
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Attribute.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

//-------------------------------------------- Constructeurs - destructeur

Attribute::Attribute()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur par défaut de <Attribute>" << endl;
#endif
} //----- Fin de Attribute

Attribute::Attribute(const Attribute &unAttribute) : id(unAttribute.id), unit(unAttribute.unit), description(unAttribute.description)
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur de copie de <Attribute>" << endl;
#endif
} //----- Fin de Attribute

Attribute::Attribute(const string &ID, const string &UNIT, const string &DESCRIPTION) : id(ID), unit(UNIT), description(DESCRIPTION)
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au constructeur  de <Attribute>" << endl;
#endif
} //----- Fin de Attribute

Attribute::~Attribute()
// Algorithme :
//
{
#ifdef OTRACE
    clog << "Appel au destructeur de <Attribute>" << endl;
#endif
} //----- Fin de ~Attribute
